import React from "react";
import { Breadcrumb } from "react-bootstrap";
import Card from "./CardProfile";
import TabProfile from "./TabProfile";

function Pasien() {
  return (
    <>
    
    <Breadcrumb
        className="pt-3 pb-2 ps-5"
        style={{ backgroundColor: "#C7EEFB" }}
      >
        <Breadcrumb.Item href="/dashboard">Beranda</Breadcrumb.Item>
        <Breadcrumb.Item active>Profile</Breadcrumb.Item>
      </Breadcrumb>
   
    <div className="container-fluid col-12 bg-dark">
      <div className="row">
        <div className="col-lg-3 pt-5 ms-4 me-4 bg-white">
          <Card />
        </div>
      
        <div className="col-lg-9 bg-white px-4 me-1 pt-5 ms-4">
          <TabProfile />
        </div>
    </div>
    </div>
  </>
  )
}

export default Pasien