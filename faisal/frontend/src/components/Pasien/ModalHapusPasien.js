import axios from "axios";
import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function ModalHapusPasien(props) {
  var data = props.value;
  const [post, setPosts] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost/minpro/getlocation/" + data)
      .then((response) => {
        setPosts(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const object = {
    Id:data,
  }

  const dataU = JSON.stringify(object)

  let handleDelete = async (e) => {
    axios.put("http://localhost/minpro/updatelocationhapus", dataU).then((res)=>{
      if(res.data.message === "Data masih terpakai"){
        handleClose()
        handleShowError()
      } else {
        handleClose()
        window.location.reload()
      }
    })
  };

  //Modal Hapus
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  //Error Modal
  const [showError, setShowError] = useState(false);
  const handleCloseError = () => setShowError(false);
  const handleShowError = () => setShowError(true);
  return (
    <div>
      {/*Delete*/}
      
        
      <Button onClick={handleShow} className='bg-white border-0'> 
      <i className="fa-solid fa-trash text-primary mr-1"></i>
      <span className="text-primary">Hapus</span>
      </Button>
      
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-white">
          <Modal.Title className="text-primary">HAPUS LOKASI</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group className="mt-4 text-center text-secondary" style={{fontSize:'18px'}}>
            <Form.Label>
              <p style={{fontWeight:'normal'}}>Anda yakin akan menghapus</p>
              {post.map((post) => {
                  return <p>'{post.Name}' ?</p>;
                })}
            </Form.Label>
          </Form.Group>

          <div className="text-center mt-4">
            <Button
              variant="white"
              onClick={handleClose}
              className="btn mr-4 border-primary text-primary"
            >
              Tidak
            </Button>
            <Button variant="primary" type="submit" onClick={handleDelete}>
              Ya
            </Button>
          </div>
        </Modal.Body>
      </Modal>
      <Modal show={showError} onHide={handleCloseError}>
        <Modal.Header className="bg-white">
          <Modal.Title className="text-primary">HAPUS LOKASI</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group className="mt-4 text-center text-secondary" style={{fontSize:'18px'}}>
            <Form.Label>
              <p style={{fontWeight:'normal'}}>Tidak dapat menghapus lokasi</p>
              {post.map((post) => {
                  return <p>'{post.Name}'</p>;
                })}
              <p style={{fontWeight:'normal'}}>Lokasi tersebut masih digunakan</p>
            </Form.Label>
          </Form.Group>

          <div className="text-center mb-2">
            
            <Button variant="primary" type="submit" onClick={handleCloseError}>
              Kembali
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalHapusPasien;
