import React, { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import axios from "axios";

function ModalTambahPasien() {
  const [post, setPosts] = useState([]);
  const [post2, setPosts2] = useState([]);
  const [Name, setName] = useState("");
  const [msg, setMsg] = useState("");
  const [msgName, setMsgName] = useState("");
  const [date, setDate] = useState("");
  const [Gender, setGender] = useState("");
  const [Height, setHeight] = useState("");
  const [Weight, setWeight] = useState("");
  const [Blood, setBlood] = useState("");
  const [Rhesus, setRhesus] = useState("");
  const [Relation, setRelation] = useState("");


  useEffect(() => {
    axios
      .get("http://localhost/minpro/getpasienblood")
      .then((response) => {
        setPosts(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
      axios
      .get("http://localhost/minpro/getpasienrelation")
      .then((response) => {
        setPosts2(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const object = 
    {
      Fullname : Name
    }

  const data = JSON.stringify(object);

  let handleSubmit = async (e) => {
      axios.post("http://localhost/minpro/addpasienbiodata", data);
      axios.get("http://localhost/minpro/getpasien/" + Name).then((response)=>{
        localStorage.setItem("bioID",JSON.stringify(response.data))
        let idBio = JSON.parse(localStorage.getItem("bioID"))
        const object2 = 
        {
          Biodata_Id : parseInt(idBio),
          Blood_Id : parseInt(Blood),
          DOB : date,
          Gender,
          Rhesus,
          Height,
          Weight
        }
        const data2 = JSON.stringify(object2);
        axios.post("http://localhost/minpro/addpasien", data2)
        let idBio2 = JSON.parse(localStorage.getItem("bioID"))
        
        console.log(idBio2)
        axios.get("http://localhost/minpro/getpasiencust/" + idBio2).then((response)=>{
          console.log(object2.Biodata_Id)
          localStorage.setItem("idCust",JSON.stringify(response.data))
          let idCust = JSON.parse(localStorage.getItem("idCust"))
          const object3 = 
          {
            Customer_Id : parseInt(idCust),
            Relation_Id : parseInt(Relation)
            
          }
          const data3 = JSON.stringify(object3);
          axios.post("http://localhost/minpro/addcustmember", data3)
          
          handleClose()
        })
      })
     
  };

  //Modal
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
    setMsg("");
    setMsgName("");
    setName("");
    setDate("");
    setGender("");
    setRhesus("");
    setHeight("");
    setWeight("");
  };
  const handleShow = () => setShow(true);
  return (
    <div>
      <Button
        onClick={handleShow}
        className="float-right border-white bg-white mt-4"
        size="sm"
      >
        <i className="fa-solid fa-plus text-primary mr-2"></i>
        <span className="text-primary">Tambah</span>
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-white">
          <Modal.Title className="text-primary">TAMBAH PASIEN</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Nama Lengkap*</b>
              </Form.Label>
              <Form.Control
                type="text"
                onChange={(e) => setName(e.target.value)}
              />
              <span className="error text-red" style={{ fontStyle: "italic" }}>
                {msgName}
              </span>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Tanggal Lahir*</b>
              </Form.Label>
              <Form.Control
                type="date"
                value={date}
                onChange={(e) => setDate(e.target.value)}
              />
              <span className="error text-red" style={{ fontStyle: "italic" }}>
                {msgName}
              </span>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Jenis Kelamin*</b>
              </Form.Label>
              <span className="error text-red" style={{ fontStyle: "italic" }}>
                {msgName}
              </span>
                {["radio"].map((type) => (
                  <div key={`inline-${type}`} className="mb-3 float-right" >
                    <Form.Check
                      inline
                      label="Pria"
                      value="M"
                      name="group1"
                      type={type}
                      onChange={(e) => setGender(e.target.value)}
                      id={`inline-${type}-1`}
                    />
                    <Form.Check
                      inline
                      label="Wanita"
                      value="F"
                      name="group1"
                      type={type}
                      onChange={(e) => setGender(e.target.value)}
                      id={`inline-${type}-2`}
                    />
                  </div>
                ))}
            </Form.Group>
            <Form.Group className="mb-2">
              <Form.Label>
                <b>Golangan Darah/Rhesus</b>
              </Form.Label>
            </Form.Group>
            <Form.Control 
            as="select" 
            className="col-5 d-inline"
            onChange={(e) => setBlood(e.target.value)}
            >
            <option hidden className="text-muted">
                  --Pilih Gol.Darah--
                </option>
                {
                post.map((post) => (
                  <option value={post.Blood_Id }>{post.Blood_Type}</option>
                ))}
            </Form.Control>
            
              <span className="error text-red" style={{ fontStyle: "italic" }}>
                {msgName}
              </span>
                {["radio"].map((type) => (
                  <div key={`inline-${type}`} className="mb-3 float-right">
                    <Form.Check
                      inline
                      label="Rh +"
                      value="Rh +"
                      name="group2"
                      type={type}
                      onChange={(e) => setRhesus(e.target.value)}
                      id={`inline-${type}-1`}
                    />
                    <Form.Check
                      inline
                      label="Rh -"
                      value="Rh -"
                      name="group2"
                      type={type}
                      onChange={(e) => setRhesus(e.target.value)}
                      id={`inline-${type}-2`}
                    />
                  </div>
                ))}
                <Form.Group className="mb-3 mt-2">
              <Form.Label>
                <b>Tinggi Badan</b>
              </Form.Label>
              <Form.Control
                type="text"
                className="col-5"
                onChange={(e) => setHeight(e.target.value)}
              />
              <span className="error text-red" style={{ fontStyle: "italic" }}>
                {msgName}
              </span>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Berat badan</b>
              </Form.Label>
              <Form.Control
                type="text"
                className="col-5"
                onChange={(e) => setWeight(e.target.value)}
              />
              <span className="error text-red" style={{ fontStyle: "italic" }}>
                {msgName}
              </span>
            </Form.Group>
            <Form.Group>
              <Form.Label>
                <b>Relasi*</b>
              </Form.Label>
            </Form.Group>
            <Form.Control 
            as="select" 
            className="mb-4"
            onChange={(e) => setRelation(e.target.value)}
            >
            <option hidden className="text-muted">
                  --Pilih Relasi--
                </option>
                {
                post2.map((post2) => (
                  <option value={post2.Relation_Id}>{post2.Relation_Name}</option>
                ))}
      
            </Form.Control>
              <span className="error text-red" style={{ fontStyle: "italic" }}>
                {msgName}
                </span>
          </Form>
          
          <div className="text-center mb-2">
              <Button
                onClick={handleClose}
                className="btn mr-2 bg-white border-primary"
                
              >
                <span style={{color : 'blue'}}>Batal</span>
              </Button>
              <Button variant="primary" type="submit" onClick={handleSubmit}>
                Simpan
              </Button>
            </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalTambahPasien;