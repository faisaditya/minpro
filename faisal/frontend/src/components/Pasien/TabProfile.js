
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Button, Form } from 'react-bootstrap';
import ModalHapusPasien from './ModalHapusPasien';
import ModalTambahPasien from './ModalTambahPasien';
import ModalUbahPasien from './ModalUbahPasien';

function TabProfile() {
  const [post, setPosts] = useState([]);
  const [search, setSearch] = useState('')
  const [isDisabled, setIsDisabled] = useState(true);
  const [checked, setChecked] = useState(false);


  useEffect(() => {
    axios.get("http://localhost/minpro/getpasien")
    .then((response) => {
      setPosts(response.data);
    })
    .catch((err) => {
      console.log(err);
    })
  }, []);

  const canBeSubmitted = () => {
    return checked ? setIsDisabled(true) : setIsDisabled(false);
  };

  const onCheckboxClick = () => {
    setChecked(!checked);
    return canBeSubmitted();
  };
  return (
    <>
    <div className="container-fluid mt-4 col-10" >
    <div className="card mb-4">
      <div className="card-header text-left container-fluid bg-white">
        <h3 className="text-primary" >DAFTAR PASIEN</h3>
      </div>
      <div className="card-body container-fluid">
      <table
      className="table table-borderless mt-3"
      id="dataTable"
      width="100%"
      cellSpacing={0}
      >
        <thead className='search-bar d-none d-sm-inline-block form-inline ml-md-1 my-2 my-md-0 mw-100 navbar-search float-left'>
        <input
               onChange={(e) => {
                setSearch(e.target.value)
              }}
              type="text"
              className="form-control border-1 border-primary small"
              placeholder="Search by nama"
              aria-label="Search"
              aria-describedby="basic-addon2"
            />
            <span className='ml-4 text-secondary opacity-20'>Urutkan</span>
            <select className="form-control border-1 border-primary small ml-2">
            <option hidden>Nama</option>
            <option>Semua</option>
            </select>
            <div>
            <Button className='float-left border-primary bg-white mt-4 mb-2' size='sm' disabled={isDisabled}>
            <i className="fa-solid fa-trash text-primary mr-2"></i>
              Hapus
            </Button>
           <ModalTambahPasien/>
            <Button className='float-right border-white bg-white mt-4' size='sm'>
             <span className='text-primary'>10</span>
            </Button>
            <Button className='float-right border-white bg-white mt-4' size='sm'>
             <span className='text-primary'> A-Z</span> 
            </Button>
            </div>
        </thead>
        <tbody>
                  {post !== null ? 
                  post.filter((val)=>{
                    if (search === ""){
                      return  val;
                    } else if(
                      val.Fullname.toLowerCase().includes(search.toLowerCase())
                    ) {
                      return val;
                    }
                  }).map((post) => {
                  return(
                    <tr>
            <td className='float-left text-left mt-4 ml-3'>
            <Form.Check type="checkbox" aria-label="checkbox 1" onClick={onCheckboxClick} />
            </td>
            <td className='float-left text-left mt-1 ml-1'>

            <h5 className='text-primary'>{post.Fullname}</h5>
            <p style={{color: 'grey', fontWeight:'lighter', fontSize:'15px'}}>
            Anak, {post.DOB} Tahun
            <br/>
            9 chat online, 5 janji dokter
            </p>
            </td>
            <td>
              <div className='row justify-content-md-center mt-4'>
              <ModalUbahPasien/>
              <ModalHapusPasien/>
              </div>
            </td>
          </tr>
                  );
                  }) : ("Data tidak ditemukan")}
          
        </tbody>
      </table>
            
      </div>
    </div>
  </div>
        </>
  );
}

export default TabProfile;