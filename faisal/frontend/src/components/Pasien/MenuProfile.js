import React from 'react'
import {Link} from 'react-router-dom'

function MenuProfile() {
  return (
    <div className='text-left pt-5 mt-5 ms-3'>
        <Link className='mb-2 text-decoration-none text-bold' to='#'>Pasien</Link><br/>
        <Link className='mb-2 text-decoration-none text-bold' to='#'>Pembelian Obat</Link><br/>
        <Link className='mb-2 text-decoration-none text-bold' to='#'>Rencana Kedatangan</Link><br/>
        <Link className='mb-2 text-decoration-none text-bold' to='#'>Riwayat Chat Dokter</Link>
    </div>
  )
}

export default MenuProfile