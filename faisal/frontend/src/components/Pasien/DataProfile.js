import React, { useEffect, useState } from "react";
import { FaPencilAlt } from "react-icons/fa";
import { Button } from "react-bootstrap";
import axios from "axios";

function DataProfile() {
  let data = localStorage.getItem("bio");

  const [fullName, setFullName] = useState("");
  const [mobilePhone, setMobilePhone] = useState("");
  const [Email, setEmail] = useState("");
  const [dob, setDob] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost/api/profile/" + data)
      .then((res) => {
        console.log(res);
        setFullName(res.data.fullname);
        setMobilePhone(res.data.nope);
        setEmail(res.data.email);
        setDob(res.data.dob);
      })
      .catch((err) => {
        console.log(err);
      });
  });
  return (
    <>
      <div className="card mb-4">
        <div className="card-header">
          <p className="card-title ms-2">Data Pribadi</p>
          <p className="text-right">
            <Button size="sm">
              <FaPencilAlt />
            </Button>
          </p>
        </div>
        <div className="card-body">
          <div className="row">
            <div className="col-sm-3">
              <p className="mb-0 ms-2 text-left">Nama Lengkap</p>
            </div>
            <div className="col-sm-9">
              <p className="text-muted mb-0 text-left">{fullName}</p>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-sm-3">
              <p className="mb-0 ms-2 text-left">Tanggal Lahir</p>
            </div>
            <div className="col-sm-9">
              <p className="text-muted mb-0 text-left">{dob}</p>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-sm-3">
              <p className="mb-0 ms-2 text-left">Nomor Handphone</p>
            </div>
            <div className="col-sm-9">
              <p className="text-muted mb-0 text-left">{mobilePhone}</p>
            </div>
          </div>
          <hr />
        </div>
      </div>
      <div className="card mb-4">
        <div className="card-header">
          <p className="card-title ms-2">Data Akun</p>
        </div>
        <div className="card-body">
          <div className="row">
            <div className="col-sm-2">
              <p className="mb-0 ms-2 text-left">Email</p>
            </div>
            <div className="col-sm-9">
              <p className="text-muted mb-0 text-left">{Email}</p>
            </div>
            <div className="col-sm-1">
              <p className="text-right">
                <Button size="sm">
                  <FaPencilAlt />
                </Button>
              </p>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-sm-2">
              <p className="mb-0 ms-2 text-left">Password</p>
            </div>
            <div className="col-sm-9">
              <p className="text-muted mb-0 text-left">******</p>
            </div>
            <div className="col-sm-1">
              <p className="text-right">
                <Button size="sm">
                  <FaPencilAlt />
                </Button>
              </p>
            </div>
          </div>
          <hr />
        </div>
      </div>
    </>
  );
}

export default DataProfile;
