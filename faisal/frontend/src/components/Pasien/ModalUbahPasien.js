import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import axios from "axios";

function ModalUbahPasien(props) {
    var data = props.value
    
    const [post, setPosts] = useState([]);
    const [Level, setLevel] = useState("");
  const [Name, setName] = useState("");
  const [msg, setMsg] = useState("");
  const [msgName, setMsgName] = useState("");
    
      useEffect(() => {
        axios.get("http://localhost/minpro/getlocation")
        .then((response) => {
          setPosts(response.data);
        })
        .catch((err) => {
          console.log(err);
        })
      }, []);

      const object = {
        Id:data,
        Name,
        Level
      }
    
      const dataU = JSON.stringify(object)
    
      let handleUpdate = async (e) => {
        axios.put("http://localhost/minpro/updatelocation", dataU);
      };
    


  //Modal
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
    setMsg("");
    setMsgName("")
    setLevel("");
  }

  const handleShow = () => setShow(true);
  return (
    <div>
      <Button className="bg-white border-0" onClick={handleShow}>
        <i className="fa-solid fa-pen text-primary mr-1"></i>
        <span className="text-primary">Ubah</span>
      </Button>
       
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-primary">
          <Modal.Title>UBAH</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Level Lokasi*</Form.Label>
              <Form.Control
                as="select"
                onChange={(e) => setLevel(e.target.value)}
              >
                <option hidden className="text-muted">
                  --Pilih--
                </option>
                {post.slice(0,3).map((post) => (
                  <option>{post.Level}</option>
                ))}
              </Form.Control>
              <span
                  className="error text-red"
                  style={{ fontStyle: "italic" }}
                >
                  {msg}
                </span>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Wilayah</b>
              </Form.Label>
              <Form.Control as="select">
                {
                post.map((post) => (
                  <option>{post.MWilayah}</option>
                ))}
              </Form.Control>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Nama*</b>
              </Form.Label>
              <Form.Control
                type="text"
                id="NAMA"
                onChange={(e) => setName(e.target.value)}
              />
              <span
                  className="error text-red"
                  style={{ fontStyle: "italic" }}
                >
                  {msgName}
                </span>
            </Form.Group>
            <div className="float-right">
              <Button
                variant="secondary"
                onClick={handleClose}
                className="btn mr-2"
              >
                Batal
              </Button>
              <Button variant="primary" type="submit" onClick={handleUpdate}>
                Simpan
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalUbahPasien