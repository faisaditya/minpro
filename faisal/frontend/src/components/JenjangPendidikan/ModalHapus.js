import axios from "axios";
import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function ModalHapus(props) {
  var data = props.value;
  const [post, setPosts] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost/minpro/geteducation/" + data)
      .then((response) => {
        setPosts(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const object = {
    ID:data,
  }

  const dataU = JSON.stringify(object)

  let handleDelete = async (e) => {
    axios.put("http://localhost/minpro/updateeducationhapus", dataU);
    handleClose();
    window.location.reload(true);
  };

  //Modal Hapus
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div>
      {/*Delete*/}
      
        
          <Button className="rounded-pill" variant="danger" onClick={handleShow}>
            Hapus
          </Button>
      
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-danger">
          <Modal.Title>HAPUS!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group className="mb-3">
            <Form.Label>
              <p>
                {" "}
                Yakin ingin menghapus jenjang pendidikan{" "}
                {post.map((post) => {
                  return <b>{post.NAME}</b>;
                })}{" "}
                ?
              </p>
            </Form.Label>
          </Form.Group>

          <div className="float-right">
            <Button
              variant="secondary"
              onClick={handleClose}
              className="btn mr-2"
            >
              Tidak
            </Button>
            <Button variant="danger" type="submit" onClick={handleDelete}>
              Ya
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalHapus;
