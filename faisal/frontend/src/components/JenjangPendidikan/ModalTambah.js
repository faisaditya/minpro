import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import axios from "axios";


function ModalTambah() {
  const [NAME, setNAME] = useState("")
  const [msg, setMsg] = useState("")

  const object = [{
    NAME
  }]

  const data = JSON.stringify(object)

  let handleSubmit = async(e) =>{
    e.preventDefault()
      if(NAME !== ""){
        axios.post("http://localhost/minpro/addeducation", data).then((res)=>{
          if(res.data.message === "Nama sudah ada di database"){
            
            setMsg("*Nama sudah ada di database")
            
          } else {
            handleClose()
            window.location.reload()
          }
        })
      } else if (NAME === "") {
        setMsg("*Nama wajib diisi")
      }
    } 

  //Modal
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false)
    setMsg("")
    setNAME("")
  }
  const handleShow = () => setShow(true);
  return (
    <div>
      <Button className="float-right mr-1" variant="success" onClick={handleShow}>
        Tambah
      </Button>


      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-primary">
          <Modal.Title>TAMBAH</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form> 
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Nama*</b>  
              </Form.Label>
              <Form.Control type="text" id="NAME" onChange={(e)=>setNAME(e.target.value)}/>
              <span className="error text-red" style={{fontStyle:"italic"}}>{msg}</span>
            </Form.Group>
            <div className="float-right">
            <Button variant="secondary"  onClick={handleClose} className='btn mr-2'>
            Batal
          </Button>
          <Button variant="primary"  onClick={handleSubmit} >
            Simpan
          </Button>
            </div>
            
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalTambah;
