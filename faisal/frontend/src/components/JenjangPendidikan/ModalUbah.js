import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import axios from "axios";

function ModalUbah(props) {
    var data = props.value
    
    const [post, setPosts] = useState([]);
    const [msg, setMsg] = useState("")
    
      useEffect(() => {
        axios.get("http://localhost/minpro/geteducation/" + data)
        .then((response) => {
          setPosts(response.data);
        })
        .catch((err) => {
          console.log(err);
        })
      }, []);


      const [NAME, setNAME] = useState("")

      const object = {
        ID:data,
        NAME
      }
    
      const dataU = JSON.stringify(object)
    
      let handleUpdate = async (e) => {
        e.preventDefault()
        if(NAME !== ""){
        axios.put("http://localhost/minpro/updateeducation", dataU).then((res)=>{
          if(res.data.message === "Nama sudah ada di database"){
            setMsg("*Nama sudah ada di database")
          } else {
            handleClose()
            window.location.reload()
          }
        })
      } else {
        setMsg("*Nama Wajib diisi")
      }
    }
    


  //Modal
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false)
    setMsg("")
    setNAME("")
  }
  const handleShow = () => setShow(true);
  return (
    <div>
      <Button className="rounded-pill" variant="primary" onClick={handleShow}>
        Ubah
      </Button>
       
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-primary">
          <Modal.Title>UBAH</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form> 
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Nama*</b>  
              </Form.Label>
              {
                  post.map((post) => {
                  return(
                    <Form.Control 
                    type="text" 
                    id="NAME" 
                    onChange={(e) => {
                        setNAME(e.target.value)
                    }} 
                    placeholder={post.NAME} 
                    />
                  );
                  })}
                  <span className="error text-red" style={{fontStyle:"italic"}}>{msg}</span>
            </Form.Group>
            <div className="float-right">
            <Button variant="secondary"  onClick={handleClose} className='btn mr-2'>
            Batal
          </Button>
          <Button variant="primary" type="submit"  onClick={handleUpdate} >
            Simpan
          </Button>
            </div>
            
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalUbah