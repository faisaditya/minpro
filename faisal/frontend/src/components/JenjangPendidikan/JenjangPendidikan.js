import axios from 'axios';
import React, { useEffect, useState } from 'react'
import ModalHapus from './ModalHapus';
import ModalTambah from './ModalTambah';
import ModalUbah from './ModalUbah';
import Layout from '../../Layout/Layout'

const JenjangPendidikan = () => {
    const [post, setPosts] = useState([]);
    const [search, setSearch] = useState('')
  
    useEffect(() => {
      axios.get("http://localhost/minpro/geteducation")
      .then((response) => {
        setPosts(response.data);
      })
      .catch((err) => {
        console.log(err);
      })
    }, []);
    
    const table = {
      border: '1px dotted black', 
    };

      return (
        <Layout>
<div className="container-fluid mt-4" >
    <div className="card shadow mb-4">
      <div className="card-header text-left container-fluid bg-primary">
        <span>JENJANG PENDIDIKAN</span>
      </div>
      <div className="card-body container-fluid">
      <form className="d-none d-sm-inline-block form-inline ml-md-1 my-2 my-md-0 mw-100 navbar-search float-left">
              <div className="search-bar">
                <input
                  onChange={(e) => {
                    setSearch(e.target.value)
                  }}
                  type="text"
                  className="form-control bg-light border-0 small"
                  placeholder="Search by nama"
                  aria-label="Search"
                  aria-describedby="basic-addon2"
                />
              </div>
      </form>
            <ModalTambah/>

        <div className="table-responsive">
          <table className="table table-borderless mt-3" id="dataTable" style={table} width="100%" cellSpacing={0}>
            <thead>
              <tr className="bg-primary">
                <th>NAMA</th>
                <th>#</th>
              </tr>
            </thead>
            <tbody>
              {post !== null ? 
                  post.filter((val)=>{
                    if (search === ""){
                      return  val;
                    } else if(
                      val.NAME.toLowerCase().includes(search.toLowerCase())
                    ) {
                      return val;
                    }
                  }).map((post) => {
                  return(
                    <tr key={post.ID}>
                      <td>{post.NAME}</td>
                      <td>
                        <div className='row justify-content-md-center'>
                        <ModalUbah value={post.ID}/>
                        <ModalHapus value={post.ID}/>
                        </div>
                        
                      </td>
                    </tr>
                  );
                  }) : ("Data tidak ditemukan")}
                
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
        </Layout>
        
      );
    }

export default JenjangPendidikan