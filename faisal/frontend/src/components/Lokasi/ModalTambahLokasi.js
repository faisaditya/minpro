import React, { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import axios from "axios";

function ModalTambahLokasi() {
  const [post, setPosts] = useState([]);
  const [post2, setPosts2] = useState([]);
  const [Level, setLevel] = useState("");
  const [Id, setId] = useState("");
  const [Name, setName] = useState("");
  const [msg, setMsg] = useState("");
  const [msgName, setMsgName] = useState("")

  useEffect(() => {
    axios
      .get("http://localhost/minpro/getlocationlevel")
      .then((response) => {
        setPosts2(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
      axios
      .get("http://localhost/minpro/getlocation")
      .then((response) => {
        setPosts(response.data);

      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  

  const object = [
    {
      Name,
      Level_id : parseInt(Level),
      Id : parseInt(Id)
    }
  ];

  const data = JSON.stringify(object);

  let handleSubmit = async (e) => {
    if (Level !== "" && Id !== "" && Name !== "") {
      axios.post("http://localhost/minpro/addlocation", data);
      handleClose();
    } else if (Level === "" && Id === "" && Name === "") {
      setMsg("*Level lokasi harus diisi");
      setMsgName("*Nama lokasi harus diisi");
      e.preventDefault();
    } else if (Level === "") {
      setMsg("*Level lokasi harus diisi");
      e.preventDefault();
    } else if (Name === "") {
      setMsgName("*Nama lokasi harus diisi");
      e.preventDefault();
    }
  };

  //Modal
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
    setMsg("");
    setMsgName("")
    setLevel("");
    setId("");
    setName("");
  };
  const handleShow = () => setShow(true);
  return (
    <div>
      <Button
        className="float-right mr-1 bg-white border-0"
        onClick={handleShow}
      >
        <i className="fa-solid fa-plus text-primary"></i>
        <span className="text-primary pl-1">Tambah</span>
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-primary">
          <Modal.Title>TAMBAH LOKASI</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Level Lokasi*</Form.Label>
              <Form.Control
                as="select"
                onChange={(e) => setLevel(e.target.value)}
              >
                <option hidden className="text-muted">
                  --Pilih--
                </option>
                {post2.map((post2) => (
                  <option value={post2.Level_id}>{post2.Level}</option>
                ))}
              </Form.Control>
              <span
                  className="error text-red"
                  style={{ fontStyle: "italic" }}
                >
                  {msg}
                </span>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Wilayah</b>
              </Form.Label>
              <Form.Control 
              as="select"
              onChange={(e) => setId(e.target.value)}
              >
                 <option hidden className="text-muted">
                  --Pilih Wilayah--
                </option>
                <option value='0'>
                  Lainnya
                </option>
                {
                post.map((post) => (
                  <option value={post.Id}>{post.MWilayah}</option>
                ))}
              </Form.Control>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Nama*</b>
              </Form.Label>
              <Form.Control
                type="text"
                onChange={(e) => setName(e.target.value)}
              />
              <span
                  className="error text-red"
                  style={{ fontStyle: "italic" }}
                >
                  {msgName}
                </span>
            </Form.Group>
            <div className="float-right">
              <Button
                variant="secondary"
                onClick={handleClose}
                className="btn mr-2"
              >
                Batal
              </Button>
              <Button variant="primary" type="submit" onClick={handleSubmit}>
                Simpan
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalTambahLokasi;
