import axios from "axios";
import React, { useEffect, useState } from "react";
import ModalHapusLokasi from "./ModalHapusLokasi";
import ModalTambahLokasi from "./ModalTambahLokasi";
import ModalUbahLokasi from "./ModalUbahLokasi";

const Lokasi = () => {
  const [post, setPosts] = useState([]);
  const [post2, setPosts2] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost/minpro/getlocation")
      .then((response) => {
        setPosts(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
      axios
      .get("http://localhost/minpro/getlocationlevel")
      .then((response) => {
        setPosts2(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="container-fluid ">
      <div className="card shadow mb-4">
        <div className="card-header text-left container-fluid bg-primary ">
          <a>Beranda</a>
          <span> / </span>
          <a>Admin</a>
          <span> / </span>
          <a>Lokasi</a>
        </div>
        <div className="card-body container-fluid">
          <div className="search-bar d-none d-sm-inline-block form-inline ml-md-1 my-2 my-md-0 mw-100 navbar-search float-left">
            <input
              onChange={(e) => {
                setSearch(e.target.value);
              }}
              type="text"
              className="form-control border-1 small"
              placeholder="Search by nama"
              aria-label="Search"
              aria-describedby="basic-addon2"
            />
            <select className="form-control border-1 small ml-4" onChange={(e) => {
                setSearch(e.target.value);
              }}>

              
            <option hidden>Level Lokasi</option>
            <option>Semua</option>
            {
            post2.map((post2)=>
            
            <option>{post2.Level}</option>
            
            )}
            </select>
            
            
          </div>

          <ModalTambahLokasi />

          <div className="table-responsive">
            <table
              className="table table-border mt-3"
              id="dataTable"
              width="100%"
              cellSpacing={0}
            >
              <thead>
                <tr className="text-primary">
                  <td>NAMA</td>
                  <td>Level Lokasi</td>
                  <td>Wilayah</td>
                  <td>#</td>
                </tr>
              </thead>
              <tbody>
              {post !== null ? 
                  post.filter((val)=>{
                    if (search === ""){
                      return  val
                    } else if(
                      val.Name.toLowerCase().includes(search.toLowerCase())
                    ) {
                      return val;
                    } else if (
                      val.Level.includes(search)
                    ) {
                      return val
                    }
                  }).map((post) => {
                  return(
                    <tr key={post.Id}>
                      <td>{post.Name}</td>
                      <td>{post.Level}</td>
                      <td>{post.Wilayah}</td>
                      <td>
                        <div className='row justify-content-md-center'>
                        <ModalUbahLokasi value={post.Id}/>
                        <ModalHapusLokasi value={post.Id}/>
                        </div>
                        
                      </td>
                    </tr>
                  );
                  }) : ("Data tidak ditemukan")}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Lokasi;
