import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import axios from "axios";

function ModalUbahLokasi(props) {
  var data = props.value;

  const [post, setPosts] = useState([]);
  const [post2, setPosts2] = useState([]);
  const [post3, setPosts3] = useState([]);
  const [Level, setLevel] = useState("");
  const [Id, setId] = useState("");
  const [Name, setName] = useState("");
  const [msg, setMsg] = useState("");
  const [msgName, setMsgName] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost/minpro/getlocation")
      .then((response) => {
        setPosts(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
    axios
      .get("http://localhost/minpro/getlocationlevel")
      .then((response) => {
        setPosts2(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
    axios
      .get("http://localhost/minpro/getlocation/" + data)
      .then((response) => {
        setPosts3(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const object = {
    Level,
    Id: parseInt(data),
    Name,
  };

  const dataU = JSON.stringify(object);

  let handleUpdate = async (e) => {
    axios.put("http://localhost/minpro/updatelocation", dataU);
  };

  //Modal
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
    setMsg("");
    setMsgName("");
    setLevel("");
    setId("");
    setName("");
  };

  const handleShow = () => setShow(true);
  return (
    <div>
      <Button className="bg-white border-0" onClick={handleShow}>
        <i className="fa-solid fa-pen text-primary"></i>
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-white">
          <Modal.Title className="text-primary">UBAH LOKASI</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Level Lokasi*</Form.Label>
              <Form.Control
                as="select"
                onChange={(e) => setLevel(e.target.value)}
              >
                <option hidden className="text-muted">
                  --Pilih--
                </option>
                {post2.map((post2) => (
                  <option>{post2.Level}</option>
                ))}
              </Form.Control>
              <span className="error text-red" style={{ fontStyle: "italic" }}>
                {msg}
              </span>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Wilayah</b>
              </Form.Label>
              <Form.Control as="select" onChange={(e) => setId(e.target.value)}>
                <option hidden className="text-muted">
                  --Pilih Wilayah--
                </option>
                <option value="0">Lainnya</option>
                {post.map((post) => (
                  <option value={post.Id}>{post.MWilayah}</option>
                ))}
              </Form.Control>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>
                <b>Nama*</b>
              </Form.Label>
              {post3.map((post3) => {
                return (
                  <Form.Control
                    type="text"
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                    placeholder={post3.Name}
                  />
                );
              })}
              <span className="error text-red" style={{ fontStyle: "italic" }}>
                {msgName}
              </span>
            </Form.Group>
            <div className="float-right">
              <Button
                variant="secondary"
                onClick={handleClose}
                className="btn mr-2"
              >
                Batal
              </Button>
              <Button variant="primary" type="submit" onClick={handleUpdate}>
                Simpan
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalUbahLokasi;
