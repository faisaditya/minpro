// import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import Home from './components/home';
// import Dashboard from './components/dashboard';

// function App() {
//   return (
//     <div className="App">
//       <Router>
//         <Routes>
//           <Route exact path="/" element={<Home/>}></Route>
//           <Route exact path="/page1" element={<Dashboard/>}></Route>
//         </Routes>
//       </Router>
//     </div>
//   );
// }

// export default App; 


import React from 'react';  
import './App.css';  
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; 
import Lokasi from './components/Lokasi/Lokasi';
import Pasien from './components/Pasien/Pasien';
import JenjangPendidikan from './components/JenjangPendidikan/JenjangPendidikan';

function App() {  
  return (  
     <div className="App">  
     <Router>
         <Routes>
           <Route exact path="/jenjangpendidikan" element={<JenjangPendidikan/>}></Route>
           <Route exact path="/lokasi" element={<Lokasi/>}></Route>
           <Route exact path="/pasien" element={<Pasien/>}></Route>
         </Routes>
       </Router>
     </div>  
  );  
}  
  
export default App; 