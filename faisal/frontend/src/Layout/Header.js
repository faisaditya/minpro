import React, { Component } from "react";
import { Link } from "react-router-dom";

class Header extends Component {
  render() {
    return (
      <>
        <nav className="navbar p-3" style={{ backgroundColor: "#C7EEFB" }}>
          <div className="container ">
            <Link to="/" className="navbar-brand text-decoration-none">
              <div className="fs-2 fw-bold text-primary">
                <img
                  src="assets/img/logo.png"
                  width="36"
                  height="36"
                  alt="AdminLTE Logo"
                  className="brand-image img-circle elevation-2 bg-primary"
                  style={{ opacity: ".8" }}
                />
                <span className="ml-2">Med.id</span>
              </div>
            </Link>
            <div class="header-middle col-sm-6">
              <div className="input-group input-group-sm">
                <input
                  className="form-control form-control-navbar"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                />
                <div className="input-group-append">
                  <button className="btn btn-navbar bg-primary" type="submit">
                    <i className="fas fa-search" />
                  </button>
                </div>
              </div>
            </div>
            <div className="header-right col-sm-2">
              <ul class="nav user-menu header-right">
                <li class="nav-item dropdown has-arrow">
                  <Link
                    to="#"
                    class="dropdown-toggle nav-link user-link"
                    data-toggle="dropdown"
                  >
                    <span class="user-img">
                      <img
                        class="rounded-circle mr-2"
                        src="assets/img/user.jpg"
                        width="40"
                        alt="img profile"
                      />
                    </span>
                    <span></span>
                  </Link>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="/profile">
                      My Profile
                    </a>
                    <a class="dropdown-item" href="/" onClick={this.onLogout}>
                      Logout
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </>
    );
  }
}

export default Header;
