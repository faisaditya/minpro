package router

import (
	"backend/controller"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {
	router := mux.NewRouter()
	hrouter := router.PathPrefix("/minpro").Subrouter() //globarl url api

	//m_education_level
	hrouter.HandleFunc("/geteducation", controller.GetEducationLevel).Methods("GET")
	hrouter.HandleFunc("/geteducation/{id}", controller.GetEducationLevelID).Methods("GET")
	hrouter.HandleFunc("/addeducation", controller.AddEducation).Methods("POST")
	hrouter.HandleFunc("/updateeducation", controller.UpdateEducationLevel).Methods("PUT")
	hrouter.HandleFunc("/updateeducationhapus", controller.UpdateEducationLevelHapus).Methods("PUT")
	hrouter.HandleFunc("/deleteeducation/{id}", controller.DeleteEducation).Methods("PUT")

	//location
	hrouter.HandleFunc("/getlocation", controller.GetLocation).Methods("GET")
	hrouter.HandleFunc("/getlocationlevel", controller.GetLocationLevel).Methods("GET")
	// hrouter.HandleFunc("/getlocationhapus", controller.GetLocationHapus).Methods("GET")
	hrouter.HandleFunc("/addlocation", controller.AddLocation).Methods("POST")
	hrouter.HandleFunc("/getlocation/{id}", controller.GetLocationID).Methods("GET")
	hrouter.HandleFunc("/updatelocationhapus", controller.UpdateLocationHapus).Methods("PUT")

	//Pasien
	hrouter.HandleFunc("/getpasien", controller.GetCustomer).Methods("GET")
	hrouter.HandleFunc("/getpasienblood", controller.GetPasienBlood).Methods("GET")
	hrouter.HandleFunc("/getpasienrelation", controller.GetPasienRelation).Methods("GET")
	hrouter.HandleFunc("/getpasien/{fullname}", controller.GetPasienFullname).Methods("GET")
	hrouter.HandleFunc("/getpasiencust/{Id}", controller.GetPasienCustId).Methods("GET")
	hrouter.HandleFunc("/addpasienbiodata", controller.AddPasienBiodata).Methods("POST")
	hrouter.HandleFunc("/addpasien", controller.AddPasien).Methods("POST")
	hrouter.HandleFunc("/addcustmember", controller.AddCustMember).Methods("POST")

	return router
}
