package controller

import (
	"backend/models"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func GetLocation(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	p, _ := json.MarshalIndent(models.GetLocation(), "", "\t")
	w.Write(p)
}

func GetLocationLevel(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	p, _ := json.MarshalIndent(models.GetLocationLevel(), "", "\t")
	w.Write(p)
}

func GetLocationID(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	muxData := mux.Vars(r)
	var id string = muxData["id"]
	p, _ := json.MarshalIndent(models.GetLocationID(id), "", "\t")
	w.Write(p)
}

func AddLocation(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var location []models.Location
	err := json.NewDecoder(r.Body).Decode(&location)
	if err != nil {
		fmt.Println("test error :", err)
	}
	models.AddLocation(location)
}

func UpdateLocationHapus(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var location_hapus models.Location
	err := json.NewDecoder(r.Body).Decode(&location_hapus)
	if err != nil {
		fmt.Println("test error :", err)
	}

	res := Response{}
	hasil := models.UpdateLocationHapus(location_hapus)
	if hasil {
		res = Response{
			Message: "Data masih terpakai",
		}
	} else {
		res = Response{
			Message: "Hapus berhasil",
		}
	}
	json.NewEncoder(w).Encode(res)
}

// func GetLocationHapus(w http.ResponseWriter, r *http.Request) {
// 	enableCors(&w)
// 	p, _ := json.MarshalIndent(models.CariLokasi(), "", "\t")
// 	w.Write(p)
// }
