package controller

import (
	"backend/models"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

type Response struct {
	ID      int64  `json:"id,omitempty"`
	Message string `json:"message,omitempty"`
}

func GetEducationLevel(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	p, _ := json.MarshalIndent(models.GetEducationLevel(), "", "\t")
	w.Write(p)
}

func GetEducationLevelID(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	muxData := mux.Vars(r)
	var id string = muxData["id"]
	p, _ := json.MarshalIndent(models.GetEducationLevelID(id), "", "\t")
	w.Write(p)
}

func AddEducation(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var education_level []models.EducationLevel
	err := json.NewDecoder(r.Body).Decode(&education_level)
	if err != nil {
		fmt.Println("test error :", err)
	}
	res := Response{}
	hasil := models.AddEducation(education_level)
	if len(hasil) == 0 {
		res = Response{
			Message: "add nama lokasi berhasil",
		}
	} else {
		res = Response{
			Message: "Nama sudah ada di database",
		}
	}
	json.NewEncoder(w).Encode(res)
}

func DeleteEducation(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	muxData := mux.Vars(r)
	var id, _ = strconv.Atoi(muxData["id"])
	models.DeleteEducation(id)
}

func UpdateEducationLevel(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var education_level models.EducationLevel
	err := json.NewDecoder(r.Body).Decode(&education_level)
	if err != nil {
		fmt.Println("test error :", err)
	}

	res := Response{}
	hasil := models.UpdateEducationLevel(education_level)
	if hasil {
		res = Response{
			Message: "Nama sudah ada di database",
		}
	} else {
		res = Response{
			Message: "Ubah data berhasil",
		}
	}
	json.NewEncoder(w).Encode(res)
}

func UpdateEducationLevelHapus(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var education_level_hapus models.EducationLevel
	err := json.NewDecoder(r.Body).Decode(&education_level_hapus)
	if err != nil {
		fmt.Println("test error :", err)
	}
	models.UpdateEducationLevelHapus(education_level_hapus)
}
