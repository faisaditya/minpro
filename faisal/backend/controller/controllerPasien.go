package controller

import (
	"backend/models"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func GetCustomer(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	p, _ := json.MarshalIndent(models.GetCustomer(), "", "\t")
	w.Write(p)
}

func GetPasienBlood(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	p, _ := json.MarshalIndent(models.GetPasienBlood(), "", "\t")
	w.Write(p)
}

func GetPasienRelation(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	p, _ := json.MarshalIndent(models.GetPasienRelation(), "", "\t")
	w.Write(p)
}

func GetPasienFullname(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	muxData := mux.Vars(r)
	var fullname string = muxData["fullname"]
	err := json.NewDecoder(r.Body).Decode(&fullname)
	if err != nil {
		fmt.Println("test error :", err)
	}
	hasil := models.GetPasienFullname(fullname)
	json.NewEncoder(w).Encode(hasil)
}

func GetPasienCustId(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	vars := mux.Vars(r)
	var custId, _ = strconv.Atoi(vars["Id"])
	err := json.NewDecoder(r.Body).Decode(&custId)
	if err != nil {
		fmt.Println("test error :", err)
	}
	hasil := models.GetPasienCustId(custId)
	json.NewEncoder(w).Encode(hasil)
}

func AddPasienBiodata(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var addpasienbio models.TambahPasien
	err := json.NewDecoder(r.Body).Decode(&addpasienbio)
	if err != nil {
		fmt.Println("test error add pasien :", err)
	}
	models.AddPasienBiodata(addpasienbio)
}

func AddPasien(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var addpasien models.TambahPasien
	err := json.NewDecoder(r.Body).Decode(&addpasien)
	if err != nil {
		fmt.Println("test error add pasien :", err)
	}
	models.AddPasien(addpasien)
}

func AddCustMember(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var addcustmember models.TambahPasien
	err := json.NewDecoder(r.Body).Decode(&addcustmember)
	if err != nil {
		fmt.Println("test error add pasien :", err)
	}
	models.AddCustMember(addcustmember)
}
