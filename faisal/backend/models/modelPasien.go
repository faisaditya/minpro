package models

import (
	"backend/config"
	"fmt"
)

type Customer struct {
	Id       int
	Fullname string
	DOB      string
}

type TambahPasien struct {
	Fullname      string
	DOB           string
	Gender        string
	Rhesus        string
	Height        string
	Weight        string
	Blood_Type    string
	Relation_Name string
	Biodata_Id    int
	Blood_Id      int
	Relation_Id   int
	Customer_Id   int
}

func GetCustomer() []Customer {
	db := config.ConnectDB()
	defer db.Close()
	var pr []Customer // buat propinsi kosong
	//sql
	sql := "select c.id,b.fullname, Extract(YEAR from AGE(now(),c.dob)) from m_customer c join m_biodata b on c.biodata_id = b.id where c.is_delete = false "
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db getCustomer:", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw Customer
		err = rows.Scan(&rw.Id, &rw.Fullname, &rw.DOB) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		pr = append(pr, rw) //data yang di ambil di append ke propinsi kosng
	}

	return pr
}

func AddPasienBiodata(addpasienbio TambahPasien) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into m_biodata(fullname,created_by,created_on) values($1,1,now())"
	_, err1 := db.Exec(sql, addpasienbio.Fullname)
	if err1 != nil {
		fmt.Println("test error addpasienBio :", err1)
	}
}

func GetPasienFullname(fullname string) int {
	db := config.ConnectDB()
	defer db.Close()
	var id int
	sql := "select id from m_biodata where lower(fullname) = lower($1) order by id desc limit 1"
	rows, err := db.Query(sql, fullname)
	if err != nil {
		fmt.Println("error cari nama : ", err)
	}
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&id)
	}
	return id
}

func GetPasienCustId(custId int) int {
	db := config.ConnectDB()
	defer db.Close()
	var id int
	sql := "select id from m_customer where biodata_id = $1 "
	rows, err := db.Query(sql, custId)
	if err != nil {
		fmt.Println("error cari nama : ", err)
	}
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&id)
	}
	return id
}

func GetPasienBlood() []TambahPasien {
	db := config.ConnectDB()
	defer db.Close()
	var pr []TambahPasien // buat propinsi kosong
	//sql
	sql := "select id,code from m_blood_group"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db getPasienBlood:", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw TambahPasien
		err = rows.Scan(&rw.Blood_Id, &rw.Blood_Type) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		pr = append(pr, rw) //data yang di ambil di append ke propinsi kosng
	}

	return pr
}

func GetPasienRelation() []TambahPasien {
	db := config.ConnectDB()
	defer db.Close()
	var pr []TambahPasien // buat propinsi kosong
	//sql
	sql := "select id,name from m_customer_relation"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db getPasienRelation:", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw TambahPasien
		err = rows.Scan(&rw.Relation_Id, &rw.Relation_Name) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		pr = append(pr, rw) //data yang di ambil di append ke propinsi kosng
	}

	return pr
}

func AddPasien(addpasien TambahPasien) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into m_customer(biodata_id,dob,gender,rhesus_type,height,weight,blood_group_id,created_by,created_on) values($1,$2,$3,$4,$5,$6,$7,1,now()) "
	_, err1 := db.Exec(sql, addpasien.Biodata_Id, addpasien.DOB, addpasien.Gender, addpasien.Rhesus, addpasien.Height, addpasien.Weight, addpasien.Blood_Id)
	if err1 != nil {
		fmt.Println("test error addpasien 1 :", err1)
	}
}

func AddCustMember(addcustmember TambahPasien) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into m_customer_member(parent_biodata_id,customer_id,customer_relation_id,created_by,created_on) values(1,$1,$2,1,now()) "
	_, err1 := db.Exec(sql, addcustmember.Customer_Id, addcustmember.Relation_Id)
	if err1 != nil {
		fmt.Println("test error addcustmember :", err1)
	}
}
