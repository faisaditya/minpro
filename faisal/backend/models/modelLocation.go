package models

import (
	"backend/config"
	"fmt"
)

type Location struct {
	Id        int
	Parent_id int
	Level_id  int
	Name      string
	Level     string
	Wilayah   string
	MWilayah  string
}

func GetLocation() []Location {
	db := config.ConnectDB()
	defer db.Close()
	var pr []Location // buat propinsi kosong
	//sql
	sql := "select ml.id,  ml.name as Nama, mll.name as Lokasi, concat(prlv.abbreviation,' ' ,prt.name) as Wilayah, concat(mll.name,' ',ml.name,', ',prlv.abbreviation,' ' ,prt.name) as Wilayah2 from m_location ml left join m_location_level mll on ml.location_level_id = mll.id left join m_location prt on prt.id = ml.parent_id left join m_location_level prlv on prlv.id = prt.location_level_id where ml.is_delete = false order by id"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db getLocation:", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw Location
		err = rows.Scan(&rw.Id, &rw.Name, &rw.Level, &rw.Wilayah, &rw.MWilayah) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		pr = append(pr, rw) //data yang di ambil di append ke propinsi kosng
	}

	return pr
}

func GetLocationLevel() []Location {
	db := config.ConnectDB()
	defer db.Close()
	var pr []Location // buat propinsi kosong
	//sql
	sql := "select id,name from m_location_level order by id"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db getLocationLevel:", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw Location
		err = rows.Scan(&rw.Level_id, &rw.Level) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		pr = append(pr, rw) //data yang di ambil di append ke propinsi kosng
	}

	return pr
}

func GetLocationID(id string) []Location {
	db := config.ConnectDB()
	defer db.Close()
	var pp []Location // buat propinsi kosong
	//sql
	sql := "select name from m_location where id = $1"
	//execute sql
	rows, err := db.Query(sql, id)
	if err != nil {
		fmt.Println("error di exec db getLocationId :", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw Location
		err = rows.Scan(&rw.Name) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data location ID:", err)
		}
		pp = append(pp, rw) //data yang di ambil di append ke propinsi kosng
	}
	return pp
}

func AddLocation(location []Location) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into m_location(name,parent_id,location_level_id,created_by,created_on) values ($1,$2,$3,1,now())"
	for i := 0; i < len(location); i++ {
		_, err := db.Exec(sql, location[i].Name, location[i].Id, location[i].Level_id)
		if err != nil {
			fmt.Println("test error AddEducation :", err)
		}
	}

}

func UpdateLocationHapus(location_hapus Location) bool {
	var hasil bool
	db := config.ConnectDB()
	defer db.Close()
	var nama int = CariLokasi(location_hapus.Id)
	if nama == 0 {
		sql := "update m_location set is_delete = true, deleted_by = 1, deleted_on = now() where id =$1"
		_, err := db.Exec(sql, location_hapus.Id)
		if err != nil {
			fmt.Println("test error hapus lokasi:", err)
		}
		hasil = false
	} else {
		hasil = true
	}
	fmt.Println(nama)
	return hasil
}

func CariLokasi(id int) int {
	var nilai int
	db := config.ConnectDB()
	defer db.Close()

	sql := "select count(name) as nilai from m_location where parent_id=$1"
	rows, err := db.Query(sql, id)
	if err != nil {
		fmt.Println("error query cari lokasi:", err)
	}
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&nilai)
	}
	fmt.Println(nilai)
	return nilai
}
