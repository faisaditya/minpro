package models

import (
	"backend/config"
	"fmt"
)

type EducationLevel struct {
	ID   int
	NAME string
}

func GetEducationLevel() []EducationLevel {
	db := config.ConnectDB()
	defer db.Close()
	var pr []EducationLevel // buat propinsi kosong
	//sql
	sql := "select id,name from m_education_level where is_delete = false order by id"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db GetEducationLevel  :", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw EducationLevel
		err = rows.Scan(&rw.ID, &rw.NAME) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		pr = append(pr, rw) //data yang di ambil di append ke propinsi kosng
	}

	return pr
}

func CariPendidikan(nama string) string {
	db := config.ConnectDB()
	defer db.Close()

	sql := "select name from m_education_level where lower(name) = lower($1) and is_delete = false limit 1 "
	rows, err := db.Query(sql, nama)
	if err != nil {
		fmt.Println("error query cari nama:", err)
	}
	defer rows.Close()
	var nm string
	for rows.Next() {
		rows.Scan(&nm)
	}
	return nm
}

func AddEducation(education_level []EducationLevel) []string {
	var hasil []string
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into m_education_level(name,created_by,created_on) values ($1,1,now())"
	for i := 0; i < len(education_level); i++ {
		var nama string = CariPendidikan(education_level[i].NAME)
		if nama == "" {
			_, err := db.Exec(sql, education_level[i].NAME)
			if err != nil {
				fmt.Println("test error AddEducation :", err)
			}
		} else {
			hasil = append(hasil, nama)
		}

	}
	return hasil
}

func DeleteEducation(id int) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "delete from m_education_level where id = $1"
	_, err := db.Exec(sql, id)
	if err != nil {
		fmt.Println("test error :", err)
	}
}

func GetEducationLevelID(id string) []EducationLevel {
	db := config.ConnectDB()
	defer db.Close()
	var pp []EducationLevel // buat propinsi kosong
	//sql
	sql := "select name from m_education_level where id = $1"
	//execute sql
	rows, err := db.Query(sql, id)
	if err != nil {
		fmt.Println("error di exec db getEducationLevelId :", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw EducationLevel
		err = rows.Scan(&rw.NAME) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		pp = append(pp, rw) //data yang di ambil di append ke propinsi kosng
	}
	return pp
}

func UpdateEducationLevel(education_level EducationLevel) bool {
	var hasil bool
	db := config.ConnectDB()
	defer db.Close()
	var nama string = CariPendidikan(education_level.NAME)
	if nama == "" {
		sql := "update m_education_level set name = $1, modified_by = 1, modified_on = now() where id =$2"
		_, err := db.Exec(sql, education_level.NAME, education_level.ID)
		if err != nil {
			fmt.Println("test error update education :", err)
		}
		hasil = false
	} else {
		hasil = true
	}
	return hasil
}

func UpdateEducationLevelHapus(education_level_hapus EducationLevel) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "update m_education_level set is_delete = true, deleted_by = 1, deleted_on = now() where id =$1"
	_, err := db.Exec(sql, education_level_hapus.ID)
	if err != nil {
		fmt.Println("test error :", err)
	}
	fmt.Println(education_level_hapus)
}
