package models

import (
	"backend/config"
	"fmt"
)

type Menu struct {
	Name       string `json:"nama_menu"`
	Url        string `json:"url_menu"`
	Big_icon   string `json:"big_icon"`
	Small_icon string `json:"small_icon"`
}

func GetDataMenuId(id string) []Menu {
	var menu1 []Menu
	db := config.ConnectDB()
	defer db.Close()
	sql := "select name, url, big_icon,small_icon from m_menu join m_menu_role on m_menu_role.menu_id = m_menu.id where m_menu_role.role_id = $1"
	row, err := db.Query(sql, id)
	if err != nil {
		fmt.Println("error Connect GetData :", err)
	}
	defer row.Close()
	for row.Next() {
		var rw Menu
		err = row.Scan(&rw.Name, &rw.Url, &rw.Big_icon, &rw.Small_icon) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		menu1 = append(menu1, rw) //data yang di ambil di append ke menu kosng
	}
	return menu1
}
