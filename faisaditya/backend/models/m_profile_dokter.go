package models

import (
	"backend/config"
	"fmt"
	"strings"
)

type Dokter struct {
	ID           int
	FOTO         string
	NAMA         string
	SPESIALISASI string
}

type Tindakan struct {
	ID       int
	TINDAKAN string
}

type AddTindakan struct {
	DOKTER_ID int
	TINDAKAN  string
	// CREATE_BY string json:"create_by"
	// CREATE_ON string json:"create_on"
}

type resTindakan struct {
	Error bool
	Text  string
}

type resTambah struct {
	Status   bool
	Tindakan resTindakan
}

type Delete struct {
	ID_TREATMENT int `json:"ID"`
	DELETE_BY    int `json:"delete_by"`
}

type Riwayat struct {
	RS              string
	LOKASI          string
	SPESIALISASI_RS string
	THN_AWAL        string
	THN_AKHIR       string
}

type Major struct {
	// Pendidikan
	INSTITUSI string
	MAJOR     string
	TAHUN     string
}

type Spesialisasi struct {
	ID   int    `json:"id_spesialisasi"`
	NAME string `json:"spesialisasi"`
}

func GetDataDokterBioId(id string) Dokter {
	var dokter1 Dokter
	db := config.ConnectDB()
	defer db.Close()
	sql := "select m_doctor.id, m_biodata.image_path as foto, m_biodata.fullname as nama, m_specialization.name as spesialisasi from m_biodata join m_doctor  on m_biodata.id=m_doctor.biodata_id join t_current_doctor_specialization on m_doctor.id=t_current_doctor_specialization.doctor_id join m_specialization on t_current_doctor_specialization.specialization_id=m_specialization.id where m_biodata.id=$1"
	row, err := db.Query(sql, id)
	if err != nil {
		fmt.Println("error Connect GetData :", err)
	}
	defer db.Close()
	for row.Next() {
		err = row.Scan(&dokter1.ID, &dokter1.FOTO, &dokter1.NAMA, &dokter1.SPESIALISASI)
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		//data yang diambil di append ke menu kosong
	}
	return dokter1
}

func UploadFile(str string, id string) {
	var foto = "/assets/img/profile/" + str
	db := config.ConnectDB()
	defer db.Close()
	sql := "update m_biodata set image_path= $1, modified_by=$2, modified_on= now() where id= $3"
	_, err := db.Exec(sql, foto, id, id)
	if err != nil {
		fmt.Println("error Query Upload :", err)
	}
}

func GetDokTindakanbyId(id string) []Tindakan {
	var tindakan []Tindakan
	db := config.ConnectDB()
	defer db.Close()
	sql := "SELECT t_doctor_treatment.id, t_doctor_treatment.name as tindakan from t_doctor_treatment join m_doctor on t_doctor_treatment.doctor_id=m_doctor.id  where m_doctor.id=$1 and t_doctor_treatment.is_delete=false"
	row, err := db.Query(sql, id)
	if err != nil {
		fmt.Println("error Connect GetData :", err)
	}
	defer db.Close()
	for row.Next() {
		var rw Tindakan
		err = row.Scan(&rw.ID, &rw.TINDAKAN)
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		tindakan = append(tindakan, rw) //data yang diambil di append ke menu kosong
	}
	return tindakan
}

// Add Tindakan
func ValidateFormTambah(data AddTindakan) resTambah {
	var result resTindakan
	respon := resTambah{}
	cekName := CekNamaTindakan(data.TINDAKAN)
	if cekName.Error {
		respon = resTambah{
			Status:   false,
			Tindakan: cekName,
		}
	} else {
		result = InsertTindakan(data)
		respon = resTambah{
			Status:   true,
			Tindakan: result,
		}
	}
	return respon
}

func InsertTindakan(add AddTindakan) resTindakan {
	var res resTindakan
	db := config.ConnectDB()
	defer db.Close()

	sql := "insert into t_doctor_treatment (doctor_id,name,created_by,created_on) values ($1,$2,1,now())"
	_, err := db.Exec(sql, add.DOKTER_ID, add.TINDAKAN)
	if err != nil {
		res = resTindakan{
			Error: true,
			Text:  "Tindakan Gagal Ditambah",
		}
		fmt.Println("sqlinserror:", err)
	} else {
		res = resTindakan{
			Error: false,
			Text:  "Tindakan Berhasil Ditambah",
		}
	}
	return res
}

func CekNamaTindakan(data string) resTindakan {
	var res resTindakan
	var getId int
	db := config.ConnectDB()
	defer db.Close()
	sql := "select id from t_doctor_treatment where lower(name) = lower($1) and is_delete=false"
	row, err := db.Query(sql, data)
	if err != nil {
		fmt.Println("errorCek:", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&getId)
	}
	fmt.Println(getId)

	if getId != 0 {
		res = resTindakan{
			Error: true,
			Text:  "Nama Tindakan Sudah Ada",
		}
	}
	return res
}

// Delete Tindakan
func DeleteTindakan(data Delete) string {
	var respon string
	db := config.ConnectDB()
	defer db.Close()
	sql := "update t_doctor_treatment set deleted_by=$1,deleted_on=now(),is_delete=true where id=$2"
	_, err := db.Exec(sql, data.DELETE_BY, data.ID_TREATMENT)
	if err != nil {
		respon = "true"
		fmt.Println("ErrorDelete:", err)
	} else {
		respon = "false"
	}
	return respon
}

func GetDokRiwayatId(id string) []Riwayat {
	var riwayat []Riwayat
	db := config.ConnectDB()
	defer db.Close()

	sql := "SELECT m_medical_facility.name as RS, m_location.name as domisili, t_doctor_office.specialization as spesialisasi_rs, t_doctor_office.created_on  as thn_awal, t_doctor_office.deleted_on as thn_akhir from t_current_doctor_specialization join m_doctor  on m_doctor.id=t_current_doctor_specialization.doctor_id join m_specialization on t_current_doctor_specialization.specialization_id=m_specialization.id join t_doctor_office on m_doctor.id=t_doctor_office.doctor_id join m_medical_facility on t_doctor_office.medical_facility_id=m_medical_facility.id  join m_location on m_location.id=m_medical_facility.location_id join m_location_level on m_location_level.id=m_location.location_level_id where  m_doctor.id=$1 ORDER BY thn_awal DESC "
	//execute sql
	rows, err := db.Query(sql, id)

	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()

	for rows.Next() {
		var rw Riwayat
		err = rows.Scan(&rw.RS, &rw.LOKASI, &rw.SPESIALISASI_RS, &rw.THN_AWAL, &rw.THN_AKHIR) //ambil data lalu di unmarshal ke STRUC
		tahunAw := rw.THN_AWAL
		tahun := rw.THN_AKHIR
		arr2 := strings.Split(tahun, "-")
		pecahTahunS1 := arr2[0]
		rw.THN_AKHIR = pecahTahunS1
		if tahun == "" {
			rw.THN_AKHIR = "sekarang"
		}
		arr1 := strings.Split(tahunAw, "-")
		pecahTahunS := arr1[0]
		rw.THN_AWAL = pecahTahunS

		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		riwayat = append(riwayat, rw)
	}
	return riwayat
}

func GetDokMajorId(id string) []Major {
	var major []Major
	db := config.ConnectDB()
	defer db.Close()

	//sql
	sql := "SELECT m_doctor_education.institution_name as institusi, m_doctor_education.major as major, m_doctor_education.start_year as tahun from m_biodata join m_doctor  on m_biodata.id=m_doctor.biodata_id join m_doctor_education on m_doctor_education.doctor_id=m_doctor.id where m_doctor.id=$1 ORDER BY tahun DESC"
	//execute sql
	rows, err := db.Query(sql, id)

	if err != nil {
		fmt.Println("error di connect getData:", err)
	}
	defer rows.Close()
	for rows.Next() {
		var rw Major
		err = rows.Scan(&rw.INSTITUSI, &rw.MAJOR, &rw.TAHUN) //ambil data lalu di unmarshal ke STRUC

		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		major = append(major, rw) //data yang diambil di append ke menu kosong
	}
	return major
}

func GetSpesialisasi() []Spesialisasi {
	db := config.ConnectDB()
	defer db.Close()
	var pr []Spesialisasi // buat propinsi kosong
	//sql
	sql := "select id,name from m_specialization where m_specialization.is_delete = false order by id"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db GETspesialisasi:", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw Spesialisasi
		err = rows.Scan(&rw.ID, &rw.NAME) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		pr = append(pr, rw) //data yang di ambil di append ke SPESIALISASI
	}

	return pr
}
