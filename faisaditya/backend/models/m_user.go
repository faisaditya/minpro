package models

import (
	"backend/config"
	"fmt"
)

type User struct {
	FullName   string `json:"fullname"`
	Image_Path string `json:"image_path"`
}

func GetDataUserId(id string) []User {
	var user1 []User
	db := config.ConnectDB()
	defer db.Close()
	sql := "select fullname,image_path from m_user join m_biodata on m_user.biodata_id = m_biodata.id where biodata_id = $1 "
	row, err := db.Query(sql, id)
	if err != nil {
		fmt.Println("error Connect GetData :", err)
	}
	defer row.Close()
	for row.Next() {
		var rw User
		err = row.Scan(&rw.FullName, &rw.Image_Path) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		user1 = append(user1, rw) //data yang di ambil di append ke menu kosng
	}
	return user1
}
