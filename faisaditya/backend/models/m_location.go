package models

import (
	"backend/config"
	"fmt"
)

type Location struct {
	Id       int
	Name     string
	Level    string
	Wilayah  string
	MWilayah string
}

func GetLocation() []Location {
	db := config.ConnectDB()
	defer db.Close()
	var pr []Location // buat propinsi kosong
	//sql
	sql := "select ml.id, ml.name as Nama, mll.name as Lokasi, concat(prlv.abbreviation,' ',prt.name) as Wilayah, concat(mll.name,' ',ml.name,', ',prlv.abbreviation,' ' ,prt.name) as Wilayah2 from m_location ml join m_location_level mll on ml.location_level_id = mll.id join m_location prt on prt.id = ml.parent_id join m_location_level prlv on prlv.id = prt.location_level_id where ml.is_delete = false order by id"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db getLocation:", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw Location
		err = rows.Scan(&rw.Id, &rw.Name, &rw.Level, &rw.Wilayah, &rw.MWilayah) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		pr = append(pr, rw) //data yang di ambil di append ke propinsi kosng
	}

	return pr
}

func GetLocationID(id string) []Location {
	db := config.ConnectDB()
	defer db.Close()
	var pp []Location // buat propinsi kosong
	//sql
	sql := "select name from m_location where id = $1"
	//execute sql
	rows, err := db.Query(sql, id)
	if err != nil {
		fmt.Println("error di exec db getLocationId :", err)
	}
	defer rows.Close()
	//iterasi rows
	for rows.Next() {
		var rw Location
		err = rows.Scan(&rw.Name) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
		pp = append(pp, rw) //data yang di ambil di append ke propinsi kosng
	}
	return pp
}


func CariLokasi() []int {
	var nilai int
	db := config.ConnectDB()
	defer db.Close()

	sql := "select ml.id from m_location ml where ml.id not in (select prt.id from m_location ml join m_location_level mll on ml.location_level_id = mll.id join m_location prt on prt.id = ml.parent_id join m_location_level prlv on prlv.id = prt.location_level_id)"
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error query cari lokasi:", err)
	}
	defer rows.Close()
	var nm []int
	for rows.Next() {
		rows.Scan(&nilai)
		nm = append(nm, nilai)
	}
	fmt.Println(nm)
	return nm
}

// "select ml.id from m_location ml where ml.id not in (select prt.id from m_location ml join m_location_level mll on ml.location_level_id = mll.id join m_location prt on prt.id = ml.parent_id join m_location_level prlv on prlv.id = prt.location_level_id)"
