package router

import (
	"backend/controllers"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {
	router := mux.NewRouter()
	//membuat subrouter
	pre := router.PathPrefix("/api").Subrouter()

	//ambil data login
	pre.HandleFunc("/user", controllers.GetDataLogin).Methods("POST")
	pre.HandleFunc("/user", controllers.GetDataLogin).Methods("GET")

	pre.HandleFunc("/getdatamenu/{id}", controllers.GetDataMenuId).Methods("GET")
	pre.HandleFunc("/getdatauser/{id}", controllers.GetDataUserId).Methods("GET")

	// Dokter Profile Route
	pre.HandleFunc("/upload", controllers.UploadFile).Methods("POST")
	pre.HandleFunc("/addtindakan", controllers.InsertTindakan).Methods("POST")
	pre.HandleFunc("/deletetindakan", controllers.DeleteTindakan).Methods("PUT")
	pre.HandleFunc("/getprofiledokter/{id}", controllers.GetDataDokterBioId).Methods("GET")
	pre.HandleFunc("/getdoktindakan/{id}", controllers.GetDokTindakanbyId).Methods("GET")
	pre.HandleFunc("/getdokriwayat/{id}", controllers.GetDokRiwayatId).Methods("GET")
	pre.HandleFunc("/getdokmajor/{id}", controllers.GetDokMajorId).Methods("GET")
	pre.HandleFunc("/getspesialisasi", controllers.GetSpesialisasi).Methods("GET")

	//location
	pre.HandleFunc("/getlocation", controllers.GetLocation).Methods("GET")
	pre.HandleFunc("/getlocation/{id}", controllers.GetLocationID).Methods("GET")

	return router
}
