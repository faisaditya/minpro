package controllers

import (
	"backend/models"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func GetDataMenuId(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	muxData := mux.Vars(r)
	var id string = muxData["id"]
	p, _ := json.MarshalIndent(models.GetDataMenuId(id), "", "\t")
	w.Write(p)
}
