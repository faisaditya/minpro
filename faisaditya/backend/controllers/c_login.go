package controllers

import (
	"backend/models"
	"encoding/json"
	"fmt"
	"net/http"
)

type Pesan struct {
	Pesan string
}

func GetDataLogin(w http.ResponseWriter, r *http.Request) {
	var user models.DataLogin
	var dataPesan = Pesan{}
	var resultPass models.Login
	json.NewDecoder(r.Body).Decode(&user)
	resultBlocked := models.GetBloked(user.Email)
	if	resultBlocked {
		dataPesan = Pesan{
			Pesan: "Akun anda Terblokir,",
		}	
		json.NewEncoder(w).Encode(dataPesan)
	} else {
			resultEmail := models.GetData(user.Email)
			Id := resultEmail.Id
			Attempt := resultEmail.Login_attempt
			count := Attempt + 1
		if Id == 0 {
			
			dataPesan = Pesan{
				Pesan: "Email tidak Terdaftar",
			}
			json.NewEncoder(w).Encode(dataPesan)

		} else {
			
			resultPass = models.GetPassword(user)
			if resultPass.Role_id == 0 {
				models.AddAttempt(Id, count)
				cekAttempt := models.GetAttempt(Id)
				fmt.Println("cek ",cekAttempt)
				dataPesan = Pesan{
				Pesan: "Password salah",}
				json.NewEncoder(w).Encode(dataPesan)
				if resultEmail.Login_attempt == 3 {
					models.IsBlocked(Id)
					dataPesan = Pesan{
						Pesan: "Akun Terblokir",
					}
				}
			} else {
				json.NewEncoder(w).Encode(resultPass)
				reset := 0
				models.AddAttempt(Id,reset)
			}
				
			
		}
				
	}
	
	// if result.Biodata_id != 0 {
	// 	json.NewEncoder(w).Encode(result)
	// } else {
	// 	dataPesan = Pesan{
	// 		Pesan: "Username/Password Salah",
	// 	}
	// 	json.NewEncoder(w).Encode(dataPesan)
	// }
}
