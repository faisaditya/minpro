package controllers

import (
	"backend/models"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

func GetLocation(w http.ResponseWriter, r *http.Request) {
	p, _ := json.MarshalIndent(models.GetLocation(), "", "\t")
	w.Write(p)
}

func GetLocationID(w http.ResponseWriter, r *http.Request) {
	muxData := mux.Vars(r)
	var id string = muxData["id"]
	p, _ := json.MarshalIndent(models.GetLocationID(id), "", "\t")
	w.Write(p)
}

// func GetLocationHapus(w http.ResponseWriter, r *http.Request) {
// 	enableCors(&w)
// 	p, _ := json.MarshalIndent(models.CariLokasi(), "", "\t")
// 	w.Write(p)
// }
