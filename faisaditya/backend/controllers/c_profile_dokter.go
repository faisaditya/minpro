package controllers

import (
	"backend/models"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/mux"
	"github.com/teris-io/shortid"
)

type Response struct {
	ID      int64  `json:"id,omitempty"`
	Message string `json:"message,omitempty"`
}

func UploadFile(w http.ResponseWriter, r *http.Request) {
	refid, _ := shortid.Generate()
	id := r.FormValue("user")
	uploadedFile, handler, err := r.FormFile("file")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer uploadedFile.Close()

	filename := handler.Filename
	if refid != "" {
		filename = fmt.Sprintf("%s%s", refid, filepath.Ext(handler.Filename))
	}
	fileLocation := filepath.Join("/Users/faisaditya/Batch300Golang/teama_batch300/faisaditya/frontend/public/assets/img/profile", filename)
	targetFile, err := os.OpenFile(fileLocation, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer targetFile.Close()

	if _, err := io.Copy(targetFile, uploadedFile); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	models.UploadFile(filename, id)
}

// Add Tindakan
func InsertTindakan(w http.ResponseWriter, r *http.Request) {
	var addTindakan models.AddTindakan
	err := json.NewDecoder(r.Body).Decode(&addTindakan)
	if err != nil {
		fmt.Println("insrolecon:", err)
	}
	result := models.ValidateFormTambah(addTindakan)
	json.NewEncoder(w).Encode(result)
}

func DeleteTindakan(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var deleteTindakan models.Delete
	err := json.NewDecoder(r.Body).Decode(&deleteTindakan)
	if err != nil {
		fmt.Println("test error :", err)
	}
	models.DeleteTindakan(deleteTindakan)
}

// Dokter name foto spesialisasi
func GetDataDokterBioId(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	muxData := mux.Vars(r)
	var id string = muxData["id"]
	p, _ := json.MarshalIndent(models.GetDataDokterBioId(id), "", "\t")
	w.Write(p)
}

func GetDokTindakanbyId(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	muxData := mux.Vars(r)
	var id string = muxData["id"]
	p, _ := json.MarshalIndent(models.GetDokTindakanbyId(id), "", "\t")
	w.Write(p)
}

func GetDokRiwayatId(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	muxData := mux.Vars(r)
	var id string = muxData["id"]
	p, _ := json.MarshalIndent(models.GetDokRiwayatId(id), "", "\t")
	w.Write(p)
}

func GetDokMajorId(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json") // set header api to jason
	muxData := mux.Vars(r)
	var id string = muxData["id"]
	p, _ := json.MarshalIndent(models.GetDokMajorId(id), "", "\t")
	w.Write(p)
}

func GetSpesialisasi(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	p, _ := json.MarshalIndent(models.GetSpesialisasi(), "", "\t")
	w.Write(p)
}
