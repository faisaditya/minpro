import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import React from "react";
import LandingPage from "./Component/LandingPage/MenuNotLogin";
import Menu from "./Component/LandingPageLogin/Menu";
import ProfileDoctor from "./Component/ProfileDoctor/ProfileDoctor";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingPage />}></Route>
        <Route path="/Dashboard" element={<Menu />}></Route>
        <Route path="/profile" element={<ProfileDoctor />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
