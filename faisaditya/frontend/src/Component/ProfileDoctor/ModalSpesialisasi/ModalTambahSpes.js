import { Modal } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import axios from "axios";

function ModalTambahSpes() {
  const data = sessionStorage.getItem("dok");
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);

  const [nameSpesialisasi, setNameSpesialisasi] = useState([]);
  // const [msg, setMsg] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost/api/getspesialisasi", data)
      .then((res) => {
        setNameSpesialisasi(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [data]);

  const handleClose = () => {
    setShow(false);
  };

  return (
    <div>
      <div class="mb-10">
        <button
          type="button"
          className="btn btn-primary rounded-circle float-end"
          onClick={handleShow}
        >
          <i className="fa fa-plus plus-circle"></i>
        </button>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Pilih Spesialisasi Anda</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div class="mb-3 ">
              <label for="name" class="form-label tindakan">
                Spesialisasi *
              </label>
              <select
                class="form-select lebar abu"
                style={{ height: 40 }}
                id="spesialisasi"
                value=""
                onChange=""
              >
                <option selected hidden>
                  --Pilih--
                </option>
                <option>{nameSpesialisasi.NAME}</option>;
              </select>
            </div>
          </Modal.Body>
          <Modal.Footer className="d-flex flex-row justify-content-center">
            <button
              type="button"
              className="btn btn-lg btn-outline-primary btn-light mr-4 px-4"
              onClick=""
            >
              Batal
            </button>
            <button type="button" className="btn btn-lg btn-primary" onClick="">
              Simpan
            </button>
          </Modal.Footer>
        </Modal>
      </div>

      <div class="mb-10">
        <Modal show="" onHide="">
          <Modal.Header closeButton>
            <Modal.Title>Sukses</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div class="modal-body text-center hapus">
              <img src="assets/img/success.png" alt="" width="50" height="46" />
              <p> </p>
              <p>Anda berhasil memperbarui Spesialis. </p>
            </div>
          </Modal.Body>

          <Modal.Footer>
            <button type="button" class="btn btn-light" onClick="cancelHandler">
              Kembali
            </button>
          </Modal.Footer>
        </Modal>
      </div>

      <div class="mb-10">
        <Modal show="" onHide="">
          <Modal.Header closeButton>
            <Modal.Title>Gagal</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div class="modal-body text-center hapus">
              <img src="assets/img/sent.png" alt="" width="50" height="46" />
              <p> </p>
              <p>Spesialis tidak boleh Kosong / Sama dengan sebelumnya. </p>
            </div>
          </Modal.Body>

          <Modal.Footer>
            <button type="button" class="btn btn-light" onClick="alertHandler">
              Kembali
            </button>
          </Modal.Footer>
        </Modal>
      </div>

      {/* Modal Edit Spesialisasi */}
      <div class="mb-10">
        <Modal show="" onHide="">
          <Modal.Header closeButton>
            <Modal.Title>Edit Spesialisasi</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div class="modal-body text-center hapus">
              <img src="assets/img/sent.png" alt="" width="50" height="46" />
              <p> </p>
              <p class="margintext">Anda setuju untuk memperbarui</p>
              <p class="margintext">Spesialis Menjadi '' ?</p>
            </div>
          </Modal.Body>

          <Modal.Footer>
            <button type="button" class="btn btn-light" onClick="cancelHandler">
              Batal
            </button>
            <button class="btn btn-danger" onClick="saveHandler">
              Yakin
            </button>
          </Modal.Footer>
        </Modal>
      </div>
    </div>
  );
}

export default ModalTambahSpes;
