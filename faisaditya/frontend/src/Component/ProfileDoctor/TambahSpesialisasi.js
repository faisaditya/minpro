import axios from "axios";
import React, { useEffect, useState } from "react";
import ModalTambahSpes from "./ModalSpesialisasi/ModalTambahSpes";

function TambahSpesialisasi() {
  const [spesialisasi, setSpesialisasi] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost/api/getspesialisasi")
      .then((res) => {
        setSpesialisasi(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  });

  return (
    <>
      <div className="tab-content mt-2">
        <div className="tab-pane show active" id="tindakan">
          {spesialisasi.map((data, index) => {
            return <div className="btn-toolbar pull-left">{data.NAME}</div>;
          })}
        </div>
        <ModalTambahSpes />
      </div>
    </>
  );
}

export default TambahSpesialisasi;
