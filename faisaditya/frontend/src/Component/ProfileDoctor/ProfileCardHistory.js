import axios from "axios";
import React, { useEffect, useState } from "react";

function ProfileCardHistory() {
  let data = sessionStorage.getItem("dok");
  const [tindakanDokter, setTindakanDokter] = useState([]);
  const [riwayat, setRiwayatDokter] = useState([]);
  const [major, setMajorDokter] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost/api/getdoktindakan/" + data)
      .then((res) => {
        setTindakanDokter(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [data]);

  console.log(tindakanDokter);

  useEffect(() => {
    axios
      .get("http://localhost/api/getdokriwayat/" + data)
      .then((res) => {
        setRiwayatDokter(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [data]);

  useEffect(() => {
    axios
      .get("http://localhost/api/getdokmajor/" + data)
      .then((res) => {
        setMajorDokter(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [data]);

  return (
    <>
      <div className="profile-info elevation-2 rounded-2 mt-3 mb-3 ">
        <div
          className="profile-about"
          style={{
            backgroundColor: "#DBF4FC",
            paddingLeft: 15,
            paddingTop: 15,
            paddingBottom: 10,
          }}
        >
          <h4 className="text-primary text-bold">Tentang Saya</h4>
        </div>

        <div style={{ padding: 15 }}>
          <h4 className="mb-3" style={{ color: "#47A6DF" }}>
            Tindakan Medis
          </h4>
          {tindakanDokter.map((tindakan, index) => {
            return (
              <p key={index}>
                <div className="ml-3 text">
                  <p>- {tindakan.TINDAKAN}</p>
                </div>
              </p>
            );
          })}

          <h4 className="mb-4" style={{ color: "#47A6DF" }}>
            Riwayat Praktek
          </h4>

          <div className="text ml-3">
            {riwayat.map((riwayat, index) => {
              return (
                <tbody key={index}>
                  <tr>
                    <td className="col-md-12 d-flex text-bold">
                      <p>
                        {riwayat.RS}, {riwayat.LOKASI}
                      </p>
                    </td>
                  </tr>
                  <tr className="text-secondary">
                    <td className="col-md-6">
                      <p>{riwayat.SPESIALISASI_RS}</p>
                    </td>
                    <td className="col-md-6">
                      <p>
                        {riwayat.THN_AWAL} - {riwayat.THN_AKHIR}
                      </p>
                    </td>
                  </tr>
                </tbody>
              );
            })}
          </div>

          <h4 className="mb-4" style={{ color: "#47A6DF" }}>
            Pendidikan
          </h4>

          <div className="ml-3">
            {major.map((majors, index) => {
              return (
                <tbody key={index}>
                  <tr>
                    <td className="col-md-12 text-bold">
                      <p>{majors.INSTITUSI}</p>
                    </td>
                  </tr>
                  <tr className="text-secondary">
                    <td className="col-md-6">
                      <p>{majors.MAJOR}</p>
                    </td>
                    <td className="col-md-6">
                      <p>{majors.TAHUN}</p>
                    </td>
                  </tr>
                </tbody>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
}

export default ProfileCardHistory;
