import axios from "axios";
import React, { useEffect, useState } from "react";
import ModalUpload from "./ModalUpload";

function ProfileCard() {
  let data = sessionStorage.getItem("bio");

  const [dokterProfile, setdokterProfile] = useState("");
  const [foto, setFoto] = useState("");
  const [spes, setSpes] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost/api/getprofiledokter/" + data)
      .then((res) => {
        setdokterProfile(res.data.NAMA);
        sessionStorage.setItem("dok", res.data.ID); //Memasukan Session untuk di lempar
        setFoto(res.data.FOTO);
        setSpes(res.data.SPESIALISASI);
      })
      .catch((err) => {
        console.log(err);
      });
  });

  return (
    <>
      {/* card Profile */}

      <div
        className="profile-info container rounded-1  elevation-2"
        style={{ marginBottom: 10, padding: 10 }}
      >
        <div className="row">
          <div className="profile-img d-flex flex-column align-content-center">
            <button className="btn btn-link" style={{ padding: 0 }} onClick="">
              <div
                className="text-secondary mb-0 "
                width="10px"
                style={{ paddingLeft: "250px" }}
              >
                <ModalUpload />
              </div>

              <img className="avatar rounded-circle" src={foto} alt="" />
            </button>
          </div>
        </div>

        <div>
          <h5
            className=""
            style={{
              textAlign: "center",
              marginTop: 30,
              color: "#47A6DF",
            }}
          >
            {dokterProfile}, Sp.A
          </h5>
          <h5 className="text-secondary" style={{ textAlign: "center" }}>
            {spes}
          </h5>
          <img
            className="mx-auto d-block"
            style={{ height: 20 }}
            src="assets/img/star.png"
            alt=""
          />
        </div>

        <div
          className="profile-about"
          style={{
            borderLeft: 0,
            borderRight: 0,
            borderRadius: 0,
            paddingBottom: 0,
          }}
        >
          <hr />
          <div className="d-flex justify-content-between text-primary text-bold mt-3 ml-3">
            Janji
            <span className="d-flex align-items-center badge badge-primary">
              9
            </span>
          </div>
          <hr />
        </div>
        <div
          className="profile-about"
          style={{
            borderLeft: 0,
            borderRight: 0,
            borderRadius: 0,
            paddingBottom: 0,
          }}
        >
          <button className="btn ">
            <h6 className="text-primary text-bold my-0">
              Obrolan / Konsultasi
            </h6>
          </button>
          <hr />
        </div>
      </div>
    </>
  );
}

export default ProfileCard;
