import axios from "axios";
import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function ModalHapus(props) {
  let dataid = sessionStorage.getItem("dok");
  var data = props.value;
  var name = props.name;
  //   const [post, setPosts] = useState("");

  //   useEffect(() => {
  //     axios
  //       .get("http://localhost/api/gettindakan/" + data)
  //       .then((response) => {
  //         setPosts(response.data);
  //         console.log(post);
  //       })
  //       .catch((err) => {
  //         console.log(err);
  //       });
  //   });

  let dokId = parseInt(dataid);
  const object = {
    ID: data,
    delete_by: dokId,
  };

  const dataU = JSON.stringify(object);
  let handleDelete = async (e) => {
    axios.put("http://localhost/api/deletetindakan", dataU);
    handleClose();
    window.location.reload(true);
  };

  //Modal Hapus
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div>
      {/*Delete*/}

      <Button
        className="btn button-xs mr-3 rounded-5 text-sm mt-3 text-primary"
        style={{ backgroundColor: "#DBF3FC" }}
        onClick={handleShow}
      >
        <i
          className="fa fa-close float-right ml-2 pt-1"
          value={data.id}
          onClick=""
        ></i>
        <tr className="text-center">
          <td>{name}</td>
        </tr>
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-primary">
          <Modal.Title>HAPUS TINDAKAN!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group className="mb-3">
            <Form.Label>
              <p>
                Anda Setuju Untuk Menghapus Tindakan <b>{name}</b>
              </p>
            </Form.Label>
          </Form.Group>

          <div className="d-flex flex-row justify-content-center">
            <Button
              onClick={handleClose}
              className="btn btn-lg btn-outline-primary btn-light mr-4 px-4"
            >
              Tidak
            </Button>
            <Button
              className="btn btn-lg"
              variant="danger"
              type="submit"
              onClick={handleDelete}
            >
              HAPUS
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalHapus;
