import axios from "axios";
import React, { useState } from "react";
import { Modal } from "react-bootstrap";

function ModalTambah() {
  const id = sessionStorage.getItem("dok");
  const [msg, setMsg] = useState("");
  const [show, setShow] = useState(false);
  const [showSukses, setShowSukses] = useState(false);
  const [toast, setToast] = useState("");
  const [errors, setErrors] = useState({});
  // const [dokter_id, setDokterId] = useState("");
  const [tindakan, setTindakan] = useState("");

  const handleClose = () => {
    setShow(false);
    setTindakan("");
    setErrors({});
  };
  const handleShowSukses = () => setShowSukses(true);
  const handleCloseSukses = () => setShowSukses(false);

  const handleTindakan = (event) => {
    setTindakan(event.target.value);
  };

  let dokId = parseInt(id);
  const data = JSON.stringify({
    dokter_id: dokId,
    tindakan: tindakan,
  });

  let Tambah = async (e) => {
    e.preventDefault();
    try {
      if (tindakan === "") {
        setMsg("* Tindakan Harus di isi");
      } else {
        axios
          .post("http://localhost/api/addtindakan", data)
          .then((result) => {
            console.log(result);
            if (!result.data.Status) {
              handleClose();
              setToast(true);
              // reload()
            } else {
              setMsg(result.data.Tindakan.Text);
              handleShowSukses();
              handleClose();
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    } catch (err) {
      console.log(err);
    }
  };

  const handleShow = () => setShow(true);

  return (
    <div>
      <button
        type="button"
        className="btn btn-primary rounded-circle float-end"
        onClick={handleShow}
      >
        <i className="fa fa-plus plus-circle"></i>
      </button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title className="text-primary">Tambah Tindakan</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <div className="mb-3 ">
            <label for="name" className="form-label">
              Tindakan *
            </label>

            <input
              type="text"
              className="form-control"
              id="NAME"
              placeholder="Masukkan Tindakan"
              value={tindakan}
              onChange={handleTindakan}
              isInvalid={errors.tindakan}
            ></input>
            <span className="error text-red" style={{ fontStyle: "italic" }}>
              {msg}
            </span>
          </div>
        </Modal.Body>
        <Modal.Footer className="d-flex flex-row justify-content-center">
          <button
            type="button"
            className="btn btn-lg btn-outline-primary btn-light mr-4 px-4"
            onClick={handleClose}
          >
            Batal
          </button>
          <button
            type="button"
            className="btn btn-lg btn-primary"
            onClick={Tambah}
          >
            Simpan
          </button>
        </Modal.Footer>
      </Modal>

      {/* Modal Suksess */}
      <Modal show={showSukses} onHide={handleCloseSukses}>
        <Modal.Header closeButton>
          <Modal.Title>Sukses</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <div class="modal-body text-center simpan">
            <img src="assets/img/success.png" alt="" width="50" height="50" />
            <p> </p>
            <p>Tindakan Berhasil Ditambhakan</p>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalTambah;
