import { Breadcrumb, Modal } from "react-bootstrap";
import Footer from "../LandingPage/Footer";
import ProfileCard from "./ProfileCard";
import ProfileCardHistory from "./ProfileCardHistory";
import TambahSpesialisasi from "./TambahSpesialisasi";
import TambahTindakan from "./TambahTindakan";

function ProfileDoctor() {
  return (
    <>
      <Breadcrumb
        style={{ backgroundColor: "#C7EEFB" }}
        className="pt-3 pb-2 ps-5"
      >
        <Breadcrumb.Item href="/dashboard">Beranda</Breadcrumb.Item>
        <Breadcrumb.Item active>Profile</Breadcrumb.Item>
      </Breadcrumb>
      <div className="card-box profile-header">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-12">
            <div className="profile-view" style={{ margin: 30 }}>
              <div className="row">
                {/* Pembungkus utama sisi kiri */}
                <div className="col-md-4">
                  {/* Profile Card */}
                  <ProfileCard />

                  {/* Card Tentang Saya */}
                  <ProfileCardHistory />
                </div>

                {/* Main */}
                <div className="col-lg-8 col-md-8 col-sm-12 col-12 elevation-2">
                  <div className="profile-info profile-tabs mt-2">
                    <ul className="nav nav-tabs nav-tabs-bottom profile-info">
                      <li className="nav-item">
                        <a
                          className="nav-link show"
                          href="#spesialisasi"
                          data-toggle="tab"
                        >
                          Spesialisasi
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link show active"
                          href="#tindakan"
                          data-toggle="tab"
                        >
                          Tindakan
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link show"
                          href="#aktivitas"
                          data-toggle="tab"
                        >
                          Aktivitas
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link show"
                          href="#konsultasi"
                          data-toggle="tab"
                        >
                          Konsultasi
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link show"
                          href="#pengaturan"
                          data-toggle="tab"
                        >
                          Pengaturan
                        </a>
                      </li>
                    </ul>

                    <div className="tab-content">
                      <div className="tab-pane show " id="spesialisasi">
                        <TambahSpesialisasi />
                      </div>
                      <div className="tab-pane show active" id="tindakan">
                        <TambahTindakan />
                      </div>
                      <div className="tab-pane show" id="aktivitas">
                        Tab Aktivitas
                      </div>
                      <div className="tab-pane show" id="konsultasi">
                        Tab Konsultasi
                      </div>
                      <div className="tab-pane show" id="pengaturan">
                        Tab Pengaturan
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="edit_photo">
        <Modal show="">
          <Modal.Header>
            <Modal.Title>Edit Photo</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="form-group">
              <label>File</label>
              <input type="file" className="form-control-file" />
              <span className="error"> "error photo"</span>
            </div>
            <div className="progress">
              <div
                className="progress-bar bg-success"
                role="progressbar"
                // style
              ></div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <button type="button" className="btn btn-primary">
              Upload
            </button>
            <button type="button" className="btn btn-primary">
              Save
            </button>
            <button type="button" className="btn btn-secondary">
              Cancel
            </button>
          </Modal.Footer>
        </Modal>
      </div>
      <div className="mb-10">
        <Modal show="" onHide="">
          <Modal.Header closeButton>
            <Modal.Title>Pemberitahuan</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div className="modal-body text-center hapus">
              <img src="assets/img/sent.png" alt="" width="50" height="46" />
              <p> </p>
              <p>Maaf, Fungsi belum tersedia. </p>
            </div>
          </Modal.Body>

          <Modal.Footer>
            <button type="button" className="btn btn-light" onClick="">
              Kembali
            </button>
          </Modal.Footer>
        </Modal>
      </div>
      <Footer />
    </>
  );
}

export default ProfileDoctor;
