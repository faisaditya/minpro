import axios from "axios";
import React, { useEffect, useState } from "react";
import ModalHapus from "./ModalTindakan/ModalHapus";
import ModalTambah from "./ModalTindakan/ModalTambah";

function TambahTindakan() {
  let data = sessionStorage.getItem("dok");
  const [dataTindakan, setDataTindakan] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost/api/getdoktindakan/" + data)
      .then((res) => {
        setDataTindakan(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [data]);

  return (
    <>
      <div className="tab-content mt-2">
        <div className="tab-pane show active" id="tindakan">
          <div className="btn-toolbar pull-left">
            {dataTindakan.map((data, index) => {
              return <ModalHapus value={data.ID} name={data.TINDAKAN} />;
            })}
          </div>
          <ModalTambah />
        </div>
      </div>
    </>
  );
}

export default TambahTindakan;
