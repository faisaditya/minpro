import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { Form } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import axios from "../Api/Api";

function Login(props) {
  console.log(props.name);
  const [show, setShow] = useState(false);
  const [see, setSee] = useState("password");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [pesan, setPesan] = useState("");
  let navigate = useNavigate();

  const obj = {
    email: email,
    password: password,
  };

  const data = JSON.stringify(obj);

  let handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (email === "") {
        setPesan("email harus di isi");
      } else if (password === "") {
        setPesan("Password Harus di isi");
      } else {
        axios.post("/user", data).then((res) => {
          setPesan(res.data.Pesan);
          setPassword("");
          if (res.data.Pesan == null) {
            let biodata = res.data.biodata_id;
            let role = res.data.role_id;
            sessionStorage.setItem("role", role);
            sessionStorage.setItem("bio", biodata);
            sessionStorage.setItem("login", true);
            navigate("/dashboard");
          }
        });
      }
    } catch (error) {
      if (!error.responses) {
        setPesan("Server not found");
      }
    }
  };

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const TogglePass = (e) => {
    e.preventDefault();
    if (see === "password") {
      setSee("text");
    } else {
      setSee("password");
    }
  };

  return (
    <>
      <Button variant="outline-secondary" onClick={handleShow}>
        Masuk
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Masuk</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form size="sm" onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                size="md"
                placeholder="abcd@mail.com"
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-4" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <div className="row">
                <div className="d-flex col-6-md">
                  <Form.Control
                    type={see}
                    size="md"
                    value={password}
                    placeholder="*******"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <Link
                    className="btn btn-outline-secondary btn-md"
                    onClick={TogglePass}
                  >
                    {see === "password" ? <FaEye /> : <FaEyeSlash />}
                  </Link>
                </div>

                <div className="col-2 ms-0"></div>
              </div>
              <span className="error">{pesan}</span>
            </Form.Group>
            <div className="text-center">
              <Button
                variant="primary"
                size="lg"
                type="submit"
                className="mb-4"
              >
                Masuk
              </Button>
              <br />
              <p className="mb-1">
                <Link className="text-decoration-none" to="/lupas">
                  Lupa Password ?
                </Link>
              </p>
              <p className="mb-1">atau</p>
              <p className="mb-1">
                Belum Memiliki Akun?
                <Link to="/daftar" className="text-decoration-none">
                  Daftar
                </Link>
              </p>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Login;
