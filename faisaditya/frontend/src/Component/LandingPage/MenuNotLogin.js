import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import LandingPage from "./LandingPage";

function MenuNotLogin() {
  let id = 3;
  const [dataMenu, setDataMenu] = useState([]);

  useEffect(() => {
    const getMenu = async () => {
      const reqData = await fetch(`http://localhost/api/getdatamenu/` + id);
      const resData = await reqData.json();
      setDataMenu(resData);
    };
    getMenu();
  }, [id]);
  return (
    <LandingPage>
      <div className="container my-5 vh-100">
        <div className="row">
          {dataMenu.map((dataMenus, index) => {
            return (
              <div
                key={index}
                className="col-lg-2 col-md-5 col-sm-5 mx-3 my-4 col-11 "
              >
                <Link to={dataMenus.url_menu}>
                  <div
                    className="card shadow p-5 rounded"
                    style={{ display: "flex", alignItems: "center" }}
                  >
                    {/* {dataMenus.big_icon}  */}
                    {/* assets/img/icon/local_shipping_black_24dp.svg */}
                    <img src={dataMenus.big_icon} alt="svg" width="50" />
                  </div>
                </Link>
                <div className="card-body">
                  <h5 className="text-center">{dataMenus.nama_menu}</h5>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </LandingPage>
  );
}
export default MenuNotLogin;
