import axios from "axios";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function Header() {
  let data = sessionStorage.getItem("bio");

  const [dataUser, setDataUser] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost/api/getdatauser/" + data)
      .then((res) => {
        setDataUser(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [data]);

  let onLogout = () => {
    localStorage.removeItem("role");
    localStorage.removeItem("bio");
    localStorage.removeItem("login");
  };

  return (
    <>
      <nav className="navbar p-2 " style={{ backgroundColor: "#C7EEFB" }}>
        <div className="container">
          <Link to="/" className="navbar-brand text-decoration-none">
            <div className="fs-2 fw-bold text-primary ml-4">
              <img
                src="assets/img/logo.png"
                width="36"
                height="36"
                alt="Logo"
                className="brand-image img-circle elevation-2 bg-primary"
                style={{ opacity: ".8" }}
              />
              <span className="d-none d-lg-inline-block ml-2">Med.id</span>
            </div>
          </Link>
          <div className="header-middle mr-5 col-lg-6 col-md-6 col-sm-6 col-7">
            <div className="input-group input-group-md">
              <input
                className="form-control form-control-navbar"
                type="search"
                placeholder="Cari Dokter atau Faskes"
                aria-label="Search"
              />
              <div className="input-group-append">
                <button className="btn btn-navbar bg-primary" type="submit">
                  <i className="fas fa-search" />
                </button>
              </div>
            </div>
          </div>
          <div className="header-right col-lg-1 col-md-2 col-sm-1 ">
            <ul className="nav user-menu header-right">
              {dataUser.map((dataUsers, index) => {
                return (
                  <li key={index} className="nav-item dropdown has-arrow">
                    <Link
                      to="#"
                      className="dropdown-toggle nav-link user-link"
                      data-toggle="dropdown"
                    >
                      <span className="user-img">
                        <img
                          className="rounded-circle mr-2"
                          src={dataUsers.image_path}
                          width="40"
                          alt="profile"
                        />
                      </span>
                      <span>Hi,{dataUsers.fullname}</span>
                    </Link>
                    <div className="dropdown-menu">
                      <Link className="dropdown-item" to="/profile">
                        My Profile
                      </Link>
                      <Link className="dropdown-item" to="/" onClick={onLogout}>
                        Logout
                      </Link>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Header;
