import axios from "axios";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Layout from "../Layout/Layout";

function Menu() {
  let data = sessionStorage.getItem("role");

  const [dataMenu, setDataMenu] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost/api/getdatamenu/" + data)
      .then((res) => {
        setDataMenu(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [data]);

  return (
    <Layout>
      <div className="container my-5 vh-100">
        <div className="row">
          {dataMenu.map((dataMenus, index) => {
            return (
              <div
                key={index}
                className="col-lg-2 col-md-5 col-sm-5 mx-3 my-4 col-11 "
              >
                <Link to={dataMenus.url_menu}>
                  <div
                    className="card shadow p-5 rounded"
                    style={{ display: "flex", alignItems: "center" }}
                  >
                    {/* {dataMenus.big_icon}  */}
                    {/* assets/img/icon/local_shipping_black_24dp.svg */}
                    <img src={dataMenus.big_icon} alt="svg" width="50" />
                  </div>
                </Link>
                <div className="card-body">
                  <h5 className="text-center">{dataMenus.nama_menu}</h5>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </Layout>
  );
}

export default Menu;
