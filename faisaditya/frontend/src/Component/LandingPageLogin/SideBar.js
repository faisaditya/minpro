import axios from "axios";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function SideBar() {
  let data = sessionStorage.getItem("role");

  const [dataMenu, setDataMenu] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost/api/getdatamenu/" + data)
      .then((res) => {
        setDataMenu(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [data]);

  return (
    <>
      <div
        className="sidebar h-100 d-none d-lg-inlineblock d-md-inline-block"
        style={{ backgroundColor: "#C7EEFB" }}
      >
        {/* Sidebar */}
        <div className="sidebar">
          {/* Sidebar Menu */}
          <nav className="mt-2">
            {dataMenu.map((item) => {
              return (
                <ul
                  key={item.id}
                  className="nav nav-pills nav-sidebar flex-column"
                  data-widget="treeview"
                  role="menu"
                  data-accordion="false"
                >
                  <li className="nav-item">
                    <Link
                      exact
                      to={item.url_menu}
                      className="nav-link bg-primary  mb-2"
                    >
                      <span
                        className="items-center"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <img
                          className="mr-2 bg-white rounded-circle p-1"
                          src={item.small_icon}
                          alt="svg"
                          width="28"
                        />
                        <span>{item.nama_menu}</span>
                      </span>
                    </Link>
                  </li>
                </ul>
              );
            })}
          </nav>
          {/* /.sidebar-menu */}
        </div>

        {/* /.sidebar */}
      </div>
    </>
  );
}

export default SideBar;
