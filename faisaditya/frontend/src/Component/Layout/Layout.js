import React from "react";
import Header from "../LandingPageLogin/Header";
import SideBar from "../LandingPageLogin/SideBar";
import Footer from "../LandingPage/Footer";

function Main(props) {
  const { children } = props;
  return (
    <div className="wrapper">
      <div id="content-wrapper" className=" d-flex flex-column">
        <div className="content">
          <Header />
          <div className="d-flex flex-row h-100">
            <div className="flex">
              <SideBar />
            </div>
            <div className="col align-self-center">{children}</div>
          </div>
          <Footer />
        </div>
      </div>
    </div>
  );
}

export default Main;
