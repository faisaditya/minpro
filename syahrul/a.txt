create table m_location (
	id serial primary key ,
	name varchar 100,
	parent_id

)

create table m_admin(
	id bigserial primary key,
	biodata_id bigint,
	code varchar (10),
	created_by bigint not null,
	created_on date not null,
	modified_by bigint,
	modified_on date,
	deleted_by bigint,
	deleted_on date,
	is_delete bool default 'false' not null
)

select * from m_admin