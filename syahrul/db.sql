PGDMP     -                	    z            teama    14.5    14.4 5    1           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            2           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            3           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            4           1262    16594    teama    DATABASE     e   CREATE DATABASE teama WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_Indonesia.1252';
    DROP DATABASE teama;
                postgres    false            �            1259    16612    m_admin    TABLE     5  CREATE TABLE public.m_admin (
    id bigint NOT NULL,
    biodata_id bigint,
    code character varying(10),
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean DEFAULT false NOT NULL
);
    DROP TABLE public.m_admin;
       public         heap    postgres    false            �            1259    16611    m_admin_id_seq    SEQUENCE     w   CREATE SEQUENCE public.m_admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.m_admin_id_seq;
       public          postgres    false    210            5           0    0    m_admin_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.m_admin_id_seq OWNED BY public.m_admin.id;
          public          postgres    false    209            �            1259    16652 	   m_biodata    TABLE     u  CREATE TABLE public.m_biodata (
    id bigint NOT NULL,
    fullname character varying(255),
    mobile_phone character varying(15),
    image bytea,
    image_path character varying(255),
    created_by bigint,
    created_on date,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on bigint,
    is_delete boolean DEFAULT false NOT NULL
);
    DROP TABLE public.m_biodata;
       public         heap    postgres    false            �            1259    16651    m_biodata_id_seq    SEQUENCE     y   CREATE SEQUENCE public.m_biodata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.m_biodata_id_seq;
       public          postgres    false    220            6           0    0    m_biodata_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.m_biodata_id_seq OWNED BY public.m_biodata.id;
          public          postgres    false    219            �            1259    16662 
   m_customer    TABLE     �  CREATE TABLE public.m_customer (
    id bigint NOT NULL,
    biodata_id bigint,
    dob date,
    gender character varying(1),
    blood_group_id bigint,
    rhesus_type character varying(5),
    height numeric,
    weight numeric,
    created_by bigint NOT NULL,
    created_on date,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean DEFAULT false NOT NULL
);
    DROP TABLE public.m_customer;
       public         heap    postgres    false            �            1259    16661    m_customer_id_seq    SEQUENCE     z   CREATE SEQUENCE public.m_customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.m_customer_id_seq;
       public          postgres    false    222            7           0    0    m_customer_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.m_customer_id_seq OWNED BY public.m_customer.id;
          public          postgres    false    221            �            1259    16636    m_menu    TABLE     �  CREATE TABLE public.m_menu (
    id bigint NOT NULL,
    name character varying(20),
    url character varying(50),
    parent_id bigint,
    big_icon character varying(100),
    small_icon character varying(100),
    created_by bigint,
    created_on date,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on bigint,
    is_delete boolean DEFAULT false NOT NULL
);
    DROP TABLE public.m_menu;
       public         heap    postgres    false            �            1259    16635    m_menu_id_seq    SEQUENCE     v   CREATE SEQUENCE public.m_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.m_menu_id_seq;
       public          postgres    false    216            8           0    0    m_menu_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.m_menu_id_seq OWNED BY public.m_menu.id;
          public          postgres    false    215            �            1259    16644    m_menu_role    TABLE       CREATE TABLE public.m_menu_role (
    id bigint NOT NULL,
    menu_id bigint,
    role_id bigint,
    created_by bigint,
    created_on date,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on bigint,
    is_delete boolean DEFAULT false NOT NULL
);
    DROP TABLE public.m_menu_role;
       public         heap    postgres    false            �            1259    16643    m_menu_role_id_seq    SEQUENCE     {   CREATE SEQUENCE public.m_menu_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.m_menu_role_id_seq;
       public          postgres    false    218            9           0    0    m_menu_role_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.m_menu_role_id_seq OWNED BY public.m_menu_role.id;
          public          postgres    false    217            �            1259    16628    m_role    TABLE     -  CREATE TABLE public.m_role (
    id bigint NOT NULL,
    name character varying(20),
    code character varying(20),
    created_by bigint,
    created_on date,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on bigint,
    is_delete boolean DEFAULT false NOT NULL
);
    DROP TABLE public.m_role;
       public         heap    postgres    false            �            1259    16627    m_role_id_seq    SEQUENCE     v   CREATE SEQUENCE public.m_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.m_role_id_seq;
       public          postgres    false    214            :           0    0    m_role_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.m_role_id_seq OWNED BY public.m_role.id;
          public          postgres    false    213            �            1259    16620    m_user    TABLE     �  CREATE TABLE public.m_user (
    id bigint NOT NULL,
    biodata_id bigint,
    role_id bigint,
    email character varying(100),
    password character varying(100),
    login_attempt integer,
    is_locked boolean,
    last_login date,
    created_by bigint,
    created_on date,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on bigint,
    is_delete boolean DEFAULT false NOT NULL
);
    DROP TABLE public.m_user;
       public         heap    postgres    false            �            1259    16619    m_user_id_seq    SEQUENCE     v   CREATE SEQUENCE public.m_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.m_user_id_seq;
       public          postgres    false    212            ;           0    0    m_user_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.m_user_id_seq OWNED BY public.m_user.id;
          public          postgres    false    211            z           2604    16615 
   m_admin id    DEFAULT     h   ALTER TABLE ONLY public.m_admin ALTER COLUMN id SET DEFAULT nextval('public.m_admin_id_seq'::regclass);
 9   ALTER TABLE public.m_admin ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    210    209    210            �           2604    16655    m_biodata id    DEFAULT     l   ALTER TABLE ONLY public.m_biodata ALTER COLUMN id SET DEFAULT nextval('public.m_biodata_id_seq'::regclass);
 ;   ALTER TABLE public.m_biodata ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    220    219    220            �           2604    16665    m_customer id    DEFAULT     n   ALTER TABLE ONLY public.m_customer ALTER COLUMN id SET DEFAULT nextval('public.m_customer_id_seq'::regclass);
 <   ALTER TABLE public.m_customer ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    221    222    222            �           2604    16639 	   m_menu id    DEFAULT     f   ALTER TABLE ONLY public.m_menu ALTER COLUMN id SET DEFAULT nextval('public.m_menu_id_seq'::regclass);
 8   ALTER TABLE public.m_menu ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    216    215    216            �           2604    16647    m_menu_role id    DEFAULT     p   ALTER TABLE ONLY public.m_menu_role ALTER COLUMN id SET DEFAULT nextval('public.m_menu_role_id_seq'::regclass);
 =   ALTER TABLE public.m_menu_role ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    218    218            ~           2604    16631 	   m_role id    DEFAULT     f   ALTER TABLE ONLY public.m_role ALTER COLUMN id SET DEFAULT nextval('public.m_role_id_seq'::regclass);
 8   ALTER TABLE public.m_role ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    214    213    214            |           2604    16623 	   m_user id    DEFAULT     f   ALTER TABLE ONLY public.m_user ALTER COLUMN id SET DEFAULT nextval('public.m_user_id_seq'::regclass);
 8   ALTER TABLE public.m_user ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    211    212            "          0    16612    m_admin 
   TABLE DATA           �   COPY public.m_admin (id, biodata_id, code, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
    public          postgres    false    210   �>       ,          0    16652 	   m_biodata 
   TABLE DATA           �   COPY public.m_biodata (id, fullname, mobile_phone, image, image_path, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
    public          postgres    false    220   �>       .          0    16662 
   m_customer 
   TABLE DATA           �   COPY public.m_customer (id, biodata_id, dob, gender, blood_group_id, rhesus_type, height, weight, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
    public          postgres    false    222   J?       (          0    16636    m_menu 
   TABLE DATA           �   COPY public.m_menu (id, name, url, parent_id, big_icon, small_icon, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
    public          postgres    false    216   �?       *          0    16644    m_menu_role 
   TABLE DATA           �   COPY public.m_menu_role (id, menu_id, role_id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
    public          postgres    false    218   �?       &          0    16628    m_role 
   TABLE DATA           �   COPY public.m_role (id, name, code, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
    public          postgres    false    214   �?       $          0    16620    m_user 
   TABLE DATA           �   COPY public.m_user (id, biodata_id, role_id, email, password, login_attempt, is_locked, last_login, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
    public          postgres    false    212   8@       <           0    0    m_admin_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.m_admin_id_seq', 1, false);
          public          postgres    false    209            =           0    0    m_biodata_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.m_biodata_id_seq', 3, true);
          public          postgres    false    219            >           0    0    m_customer_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.m_customer_id_seq', 1, true);
          public          postgres    false    221            ?           0    0    m_menu_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.m_menu_id_seq', 1, false);
          public          postgres    false    215            @           0    0    m_menu_role_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.m_menu_role_id_seq', 1, false);
          public          postgres    false    217            A           0    0    m_role_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.m_role_id_seq', 3, true);
          public          postgres    false    213            B           0    0    m_user_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.m_user_id_seq', 3, true);
          public          postgres    false    211            �           2606    16618    m_admin m_admin_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.m_admin
    ADD CONSTRAINT m_admin_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.m_admin DROP CONSTRAINT m_admin_pkey;
       public            postgres    false    210            �           2606    16660    m_biodata m_biodata_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.m_biodata
    ADD CONSTRAINT m_biodata_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.m_biodata DROP CONSTRAINT m_biodata_pkey;
       public            postgres    false    220            �           2606    16670    m_customer m_customer_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.m_customer
    ADD CONSTRAINT m_customer_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.m_customer DROP CONSTRAINT m_customer_pkey;
       public            postgres    false    222            �           2606    16642    m_menu m_menu_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.m_menu
    ADD CONSTRAINT m_menu_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.m_menu DROP CONSTRAINT m_menu_pkey;
       public            postgres    false    216            �           2606    16650    m_menu_role m_menu_role_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.m_menu_role
    ADD CONSTRAINT m_menu_role_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.m_menu_role DROP CONSTRAINT m_menu_role_pkey;
       public            postgres    false    218            �           2606    16634    m_role m_role_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.m_role
    ADD CONSTRAINT m_role_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.m_role DROP CONSTRAINT m_role_pkey;
       public            postgres    false    214            �           2606    16626    m_user m_user_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.m_user
    ADD CONSTRAINT m_user_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.m_user DROP CONSTRAINT m_user_pkey;
       public            postgres    false    212            "      x������ � �      ,   q   x�3�tK�,VpL�,�L�4�053112742��!cN###]C]C��qr:��z�5Y�������3JJ
8q�2�,ȬL�SH�+O,�5533���k�[o� G(D      .   8   x�3�4�4��4�50"�NC�?NCsN3S�����������%H�Ҹb���� M�
�      (      x������ � �      *      x������ � �      &   \   x�3���IUpL������q�wt����4�4202�54�5����C�4.#������T�� �`OW���!�\�KR� �\��C\��i����� �b'8      $   g   x�3�4�Ĕ��<0�����Y�X\�i�����5202�54�5�@��$�(�ˈK�S�@����r����)�0�Ɯ ���]4 B�jD� �1�     