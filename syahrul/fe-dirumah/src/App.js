
import './App.css';
import { Routes, Route, BrowserRouter } from "react-router-dom";
import Menu from './Component/LandingPageLogin/Menu'
import LupaPassword from './Component/LupaPassword/LupaPassword'
import Daftar from './Component/Daftar/Daftar'
import MenuNotLogin from './Component/LandingPage/MenuNotLogin';
import Profile from './Component/Profile/Profile';

function App() {
  return (
    <div className="App">
     <BrowserRouter>
        <Routes>
          <Route path="/" element={<MenuNotLogin />} />
          <Route path='/dashboard' element={<Menu />}/>
          <Route path="/lupas" element={<LupaPassword />} />
          <Route path="/daftar" element={<Daftar />} />
          <Route path='/profile' element={<Profile/>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
