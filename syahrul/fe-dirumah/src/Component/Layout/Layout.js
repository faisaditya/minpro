import React from "react";
import Header from "../LandingPageLogin/Header";
import SideBar from "../LandingPageLogin/SideBar";
import { useNavigate } from 'react-router-dom';

function Main(props) {
  const dataLogin = sessionStorage.getItem('login')
  const nav = useNavigate()
  if(dataLogin === false){
    nav("/")
  }

  const { children } = props;
  return (
    <div className="wrapper">
      <div id="content-wrapper" className=" d-flex flex-column">
        <div className="content">
          <Header />
          <div className="d-flex flex-row">
            <div className="flex">
              <SideBar />
            </div>
            <div className="col align-self-center">
              {children}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Main;
