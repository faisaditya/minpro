import React from "react";
import Card from "react-bootstrap/Card";
import { Button, Image } from "react-bootstrap";
import { FaStar } from "react-icons/fa";  
import MenuProfile from "./MenuProfile";
import { FaPencilAlt } from "react-icons/fa";

function CardProfile() {
  return (
    <>
      <Card>
        <p className="text-right me-3 mt-2"><Button size="sm"><FaPencilAlt/></Button></p>
        <Image
          variant="top"
          roundedCircle
          src="/logo192.png"
        />

        <Card.Body>
          <FaStar />
          <h5>Nama Pasien</h5>
          <p className="h6">Bronze Member</p>
          <p className="h6">Since 2020</p>

          <MenuProfile />
        </Card.Body>
      </Card>
    </>
  );
}

export default CardProfile;
