import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import DataProfile from "./DataProfile";


function TabProfile() {
  return (
    <Tabs
      defaultActiveKey="profile"
      id="uncontrolled-tab-example"
      className="mb-3"
    >
    <Tab eventKey="profile" title="Profile" >
        <DataProfile />          
      </Tab>
      <Tab eventKey="alamat" title="Alamat"></Tab>
      <Tab eventKey="pembayaran" title="Pembayaran"></Tab>
    </Tabs>
  );
}

export default TabProfile;
