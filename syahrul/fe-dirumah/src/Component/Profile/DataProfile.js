import React from "react";
import { Button } from "react-bootstrap";
import { FaPencilAlt } from "react-icons/fa";

function DataProfile() {
  return (
    <>
      <div className="card mb-4">
        <div className="card-header">
          <p className="card-title ms-2">Data Pribadi</p>
          <p className="text-right"><Button size="sm"><FaPencilAlt/></Button></p>
        </div>
      <div className="card-body">
        <div className="row">
          <div className="col-sm-3">
            <p className="mb-0 ms-2 text-left">Nama Lengkap</p>
          </div>
          <div className="col-sm-9">
            <p className="text-muted mb-0 text-left">Johnatan Smith</p>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-sm-3">
            <p className="mb-0 ms-2 text-left">Tanggal Lahir</p>
          </div>
          <div className="col-sm-9">
            <p className="text-muted mb-0 text-left">example@example.com</p>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-sm-3">
            <p className="mb-0 ms-2 text-left">Nomor Handphone</p>
          </div>
          <div className="col-sm-9">
            <p className="text-muted mb-0 text-left">(097) 234-5678</p>
          </div>
        </div>
        <hr />
      </div>
    </div>
      <div className="card mb-4">
        <div className="card-header">
          <p className="card-title ms-2">Data Akun</p>
          
        </div>
      <div className="card-body">
        <div className="row">
          <div className="col-sm-2">
            <p className="mb-0 ms-2 text-left">Email</p>
          </div>
          <div className="col-sm-9">
            <p className="text-muted mb-0 text-left">Johnatan Smith</p>
          </div>
          <div className="col-sm-1">
            <p className="text-right"><Button size="sm"><FaPencilAlt/></Button></p>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-sm-2">
            <p className="mb-0 ms-2 text-left">Password</p>
          </div>
          <div className="col-sm-9">
            <p className="text-muted mb-0 text-left">example@example.com</p>
          </div>
          <div className="col-sm-1">
          <p className="text-right"><Button size="sm"><FaPencilAlt/></Button></p>
          </div>
        </div>
        <hr />
      </div>
    </div>
            
    </>
  );
}

export default DataProfile;
