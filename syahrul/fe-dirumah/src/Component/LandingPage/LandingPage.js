import React from "react";
import HeaderNotLogin from "./HeaderNotLogin";
import { useNavigate } from 'react-router-dom';

function LandingPage(props) {
  const dataLogin = sessionStorage.getItem('login')
  const navigate = useNavigate()
  if (dataLogin === true){
    navigate('/dashboard')
  }



  const { children } = props;
  return (
        <div className="wrapper">
          <div id="content-wrapper" className=" d-flex flex-column">
            <div className="content">
              <HeaderNotLogin />
              <div className="d-flex flex-row">
              <div className="col align-self-center">
                {children}
              </div>
              </div>
            </div>
          </div>
        </div>

  );
}

export default LandingPage;
