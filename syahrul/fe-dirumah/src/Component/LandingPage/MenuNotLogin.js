import React, { Component } from "react";
import { Link } from "react-router-dom";
import LandingPage from "./LandingPage";

export default class MenuNotLogin extends Component {
  render() {
    return (
      <LandingPage>
        <div className="container my-5">
          <div className="row">
            {/* card 1  */}
            <div className="col-3 mb-5">
              <Link to="">
                <div className="card shadow p-5 mb-3 bg-body rounded">
                  <img
                    className="rounded-circle mx-auto d-block mt-4"
                    src="assets/img/icon/search_black_48dp.svg"
                    width="100"
                    alt="Admin"
                  />
                </div>
              </Link>
              <div className="card-body mb-3">
                <h5 className="text-center">Nama Menu</h5>
              </div>
            </div>

            {/* card 2  */}
            <div className="col-3">
              <Link to="" onClick="">
                <div className="card kartu shadow p-5 mb-3 bg-body rounded">
                  <img
                    className="rounded-circle mx-auto d-block mt-4"
                    src="assets/img/icon/search_black_48dp.svg"
                    width="100"
                    alt="Admin"
                  />
                </div>
              </Link>
              <div className="card-body mb-3">
                <h5 className="text-center">Nama Menu</h5>
              </div>
            </div>

            {/* card 2  */}
            <div className="col-3">
              <Link to="" onClick="">
                <div className="card kartu shadow p-5 mb-3 bg-body rounded">
                  <img
                    className="rounded-circle mx-auto d-block mt-4"
                    src="assets/img/icon/search_black_48dp.svg"
                    width="100"
                    alt="Admin"
                  />
                </div>
              </Link>
              <div className="card-body mb-3">
                <h5 className="text-center">Nama Menu</h5>
              </div>
            </div>

            {/* card 2  */}
            <div className="col-3">
              <Link to="" onClick="">
                <div className="card kartu shadow p-5 mb-3 bg-body rounded">
                  <img
                    className="rounded-circle mx-auto d-block mt-4"
                    src="assets/img/icon/search_black_48dp.svg"
                    width="100"
                    alt="Admin"
                  />
                </div>
              </Link>
              <div className="card-body mb-3">
                <h5 className="text-center">Nama Menu</h5>
              </div>
            </div>

            {/* card 2  */}
            <div className="col-3">
              <Link to="" onClick="">
                <div className="card kartu shadow p-5 mb-3 bg-body rounded">
                  <img
                    className="rounded-circle mx-auto d-block mt-4"
                    src="assets/img/icon/search_black_48dp.svg"
                    width="100"
                    alt="Admin"
                  />
                </div>
              </Link>
              <div className="card-body mb-3">
                <h5 className="text-center">Nama Menu</h5>
              </div>
            </div>

            {/* card 2  */}
            <div className="col-3">
              <Link to="" onClick="">
                <div className="card kartu shadow p-5 mb-3 bg-body rounded">
                  <img
                    className="rounded-circle mx-auto d-block mt-4"
                    src="assets/img/icon/search_black_48dp.svg"
                    width="100"
                    alt="Admin"
                  />
                </div>
              </Link>
              <div className="card-body mb-3">
                <h5 className="text-center">Nama Menu</h5>
              </div>
            </div>

            {/* card 2  */}
            <div className="col-3">
              <Link to="" onClick="">
                <div className="card kartu shadow p-5 mb-3 bg-body rounded">
                  <img
                    className="rounded-circle mx-auto d-block mt-4"
                    src="assets/img/icon/search_black_48dp.svg"
                    width="100"
                    alt="Admin"
                  />
                </div>
              </Link>
              <div className="card-body mb-3">
                <h5 className="text-center">Nama Menu</h5>
              </div>
            </div>

            {/* card 2  */}
            <div className="col-3">
              <Link to="" onClick="">
                <div class="card kartu shadow p-5 mb-3 bg-body rounded">
                  <img
                    className="rounded-circle mx-auto d-block mt-4"
                    src="assets/img/icon/search_black_48dp.svg"
                    width="100"
                    alt="Admin"
                  />
                </div>
              </Link>
              <div className="card-body mb-3">
                <h5 className="text-center">Nama Menu</h5>
              </div>
            </div>
          </div>
        </div>
      </LandingPage>
    );
  }
}
