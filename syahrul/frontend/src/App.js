import "./App.css";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import Menu from "./Component/LandingPageLogin/Menu";
import Login from "./Component/Login/Login";
import LupaPassword from "./Component/LupaPassword/LupaPassword";
import MenuNotLogin from "./Component/LandingPage/MenuNotLogin";
import Profile from "./Component/Profile/Profile";
import CountDown from "./Component/Login/CountDown";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MenuNotLogin />} />
          <Route path="/dashboard" element={<Menu />} />
          <Route path="/lupas" element={<LupaPassword />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/countdown" element={<CountDown/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
