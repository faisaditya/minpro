import React, { useState, useEffect, useRef } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { Alert, Form, InputGroup } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import axios from "../Api/Api";

import validator from "validator";

function Login(props) {
  const [post, setPost] = useState([]);
  const [show, setShow] = useState(false);
  const [showLupas, setShowLupas] = useState(false);
  const [showReg, setShowReg] = useState(false);
  const [showOtp, setShowOtp] = useState(false);
  const [showOtpLupas, setShowOtpLupas] = useState(false);
  const [showPw, setShowPw] = useState(false);
  const [showPwLupas, setShowPwLupas] = useState(false);
  const [showBio, setShowBio] = useState(false);
  const [see, setSee] = useState("password");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [pesanEmail, setPesanEmail] = useState("");
  const [pesanPass, setPesanPass] = useState("");
  const [pesan, setPesan] = useState("");
  const [alert1, setAlert1] = useState(false);
  const [alert2, setAlert2] = useState(false);
  const [alert3, setAlert3] = useState(false);
  const [alertPass, setAlertPass] = useState(false);
  const [alertPassLupas, setAlertPassLupas] = useState(false);
  const [pesanPassReg, setPesanPassReg] = useState("");
  const [pesanPassLupas, setPesanPassLupas] = useState("");
  const [cekMail, setCekMail] = useState("");
  const [cekMailLupas, setCekMailLupas] = useState("");
  const [pesanCekMail, setPesanCekMail] = useState("");
  const [pesanLupas, setPesanLupas] = useState("");
  const [alertReg, setAlertReg] = useState(false);
  const [alertLupas, setAlertLupas] = useState(false);
  const [alertOtp, setAlertOtp] = useState("");
  const [alertOtpLupas, setAlertOtpLupas] = useState("");
  const [pesanAlertOtp, setPesanAlertOtp] = useState("");
  const [pesanAlertOtpLupas, setPesanAlertOtpLupas] = useState("");
  const [alertPw, setAlertPw] = useState("");
  const [idRole, setIdRole] = useState("");
  const [dataOtp, setDataOtp] = useState("");
  const [dataOtpLupas, setDataOtpLupas] = useState("");
  const [passwordReg1, setPasswordReg1] = useState("");
  const [passwordReg2, setPasswordReg2] = useState("");
  const [passwordLupas1, setPasswordLupas1] = useState("");
  const [passwordLupas2, setPasswordLupas2] = useState("");
  const [nope, setNope] = useState("");
  const [fullname, setFullname] = useState("");

  let navigate = useNavigate();
  const obj = {
    email: email,
    password: password,
  };

  const data = JSON.stringify(obj);

  let handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (email === "" && password === "" ) {
        setPesanEmail("email Harus Terisi");
        setAlert1(true);
        setPesanPass("Password Harus Terisi");
        setAlert2(true);
      } else if (email === "") {
        setPesanEmail("Email Harus Terisi");
        setAlert1(true);
        setAlert2(false)
        setPesanPass("")
      } else if (!validator.isEmail(email)) {
         setPesanEmail("Please include an '@' in this email address '"+email+"' is missing an '@'");
         setAlert1(true);
         setAlert2(false);
         setPesanPass("");
      }else if (password === "") {
        setPesanPass("Password Harus Terisi");
        setAlert2(true);
        setAlert1(false);
        setPesanEmail("")
      } else {
        axios.post("/user", data).then((res) => {
          setPesan(res.data.Pesan);
          setAlert3(true);
          setPassword("");
          setPesanEmail("")
          setAlert1(false);
          setPesanPass("")
          setAlert2(false);
          if (res.data.Pesan == null) {
            let biodata = res.data.biodata_id;
            let role = res.data.role_id;
            localStorage.setItem("role", role);
            localStorage.setItem("bio", biodata);
            localStorage.setItem("login", true);
            navigate("/dashboard");
          }
        });
      }
    } catch (error) {
      if (!error.responses) {
        setPesan("Server not found");
      }
    }
  };

  const handleClose = () => {
    setShow(false);
    setEmail("")
    setPassword("")
    setAlert1(false)
    setAlert2(false)
    setAlert3(false)
  }
  const handleShow = () => setShow(true);
  const handleCloseLupas = () => setShowLupas(false);
  const handleShowLupas = () => setShowLupas(true);
  const handleCloseOtp = () => setShowOtp(false);
  const handleCloseOtpLupas = () => setShowOtpLupas(false);
  const handleShowOtp = () => setShowOtp(true);
  const handleShowOtpLupas = () => setShowOtpLupas(true);
  const handleClosePw = () => setShowPw(false);
  const handleShowPw = () => setShowPw(true);
  const handleClosePwLupas = () => setShowPwLupas(false);
  const handleShowPwLupas = () => setShowPwLupas(true);
  const handleCloseReg = () => setShowReg(false);
  const handleShowReg = () => setShowReg(true);
  const handleCloseBio = () => setShowBio(false);
  const handleShowBio = () => setShowBio(true);
  const TogglePass = (e) => {
    e.preventDefault();
    if (see === "password") {
      setSee("text");
    } else {
      setSee("password");
    }
  };
  const handleLinkShow = () => {
    handleShowReg();
    handleClose();
  };

  const cekData = {
    Email: cekMail,
  };
  const cek = JSON.stringify(cekData);

  const handleCekEmail = (e) => {
    e.preventDefault();
    try {
      if (cekMail == "") {
        setPesanCekMail("Harap Mengisi Email");
        setAlertReg(true);
      } else if (!validator.isEmail(cekMail)) {
        setPesanCekMail("Please include an '@' in this email address '"+cekMail+"' is missing an '@'")
      }else {
        axios.post("/cekemail", cek).then((res) => {
          if (res.data.Status == true) {
            setPesanCekMail(res.data.Pesan);
            setAlertReg(true);
          } else {
            handleCloseReg();
            handleShowOtp();
          }
        });
      }
    } catch (error) {
      if (!error.responses) {
        setPesan("Server not found");
      }
    }
  };
  let convert = parseInt(dataOtp);
  const cekDataOtp = {
    Otp: convert,
    Email: cekMail,
  };
  const cekOtp = JSON.stringify(cekDataOtp);

  const handleOtp = (e) => {
    e.preventDefault();
    try {
      if (dataOtp == "") {
        setAlertOtp(true);
        setPesanAlertOtp("Kode OTP tidak boleh kosong");
      } else {
        console.log(cekOtp);
        axios.post("/cekotp", cekOtp).then((res) => {
          if (res.data.Status == true) {
            setPesanAlertOtp(res.data.Pesan);
            setAlertOtp(true);
          } else {
            handleCloseOtp();
            handleShowPw();
          }
        });
      }
    } catch (error) {
      if (!error.responses) {
        setPesanAlertOtp("Server not found");
      }
    }
  };

  const resend = (e) => {
    e.preventDefault();
    try {
      axios.post("/resend", cek).then((res) => {
        setPesanAlertOtp(res.data.Pesan);
        setAlertOtp(true);
      });
    } catch (error) {
      if (!error.responses) {
        setPesanAlertOtp("Server not found");
        setAlertOtp(true);
      }
    }
  };


  let dataPass = JSON.stringify({
    Email: cekMail,
    Password: passwordReg1,
  });

  const handleBio = (e) => {
    e.preventDefault();
    try {
      if (passwordReg1 === "") {
        setPesanPassReg("Harapa Mengisi Password terlebih dahulu")
      } else if (passwordReg2 === "") {
        setPesanPassReg("Harap Mengisi Konfirmasi Password terlebih dahulu")
      } else if (!validator.isStrongPassword(passwordReg1, { minLength: 8 })) {
        setPesanPassReg("Password Harus 8 karakter")
      }

      if (passwordReg1 == passwordReg2) {
        if (
          validator.isStrongPassword(passwordReg1, {
            minLength: 8,
            minLowercase: 1,
            minUppercase: 1,
            minNumbers: 1,
            minSymbols: 1,
          })
        ) {
          axios.post("/setpass", dataPass);
          handleClosePw();
          handleShowBio();
        } else {
          setAlertPass(true);
          setPesanPassReg("Password Terlalu Pendek");
        }
      } else {
        setAlertPass(true);
        setPesanPassReg("Password tidak sama");
      }
    } catch (error) {
      if (!error.responses) {
        setPesanPassReg("Server not found");
      }
    }
  };

  useEffect(() => {
    axios
      .get("/getrole")
      .then((res) => {
        setPost(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let nomortlp = "0" + nope;
  let role = parseInt(idRole);

  let dataBio = JSON.stringify({
    Email: cekMail,
    Fullname: fullname,
    Mobile_phone: nomortlp,
    Role_id: role,
  });

  const handleSubmitBio = (e) => {
    try {
      axios.post("/setbio", dataBio);
    } catch (error) {
      if (!error.responses) {
        setPesanPassReg("Server not found");
      }
    }
  };

  const handleLinkShowLupas = () => {
    handleShowLupas();
    handleClose();
  };

  let dataCekLupas = JSON.stringify({
    Email: cekMailLupas,
  });

  const handleCekMailLupas = (e) => {
    e.preventDefault();
    try {
      if (cekMailLupas == "") {
        setPesanLupas("Harap Mengisi Email");
        setAlertLupas(true);
      } else {
        axios.post("/ceklupas", dataCekLupas).then((res) => {
          if (res.data.Status == true) {
            setPesanLupas(res.data.Pesan);
            setAlertLupas(true);
          } else {
            handleCloseLupas();
            handleShowOtpLupas();
          }
        });
      }
    } catch (error) {
      if (!error.responses) {
        setPesan("Server not found");
      }
    }
  };
  let convertOtp = parseInt(dataOtpLupas)
  let OtpLupas = JSON.stringify({
    Otp: convertOtp,
    Email: cekMailLupas,
  });
 
  
  const handleCekOtpLupas = (e) => {
    e.preventDefault();
    try {
      if (dataOtpLupas == "") {
        setAlertOtpLupas(true);
        setPesanAlertOtpLupas("Kode OTP tidak boleh kosong");
      } else {
        axios.post("/cekotplupas", OtpLupas).then((res) => {
          if (res.data.Status == true) {
            setPesanAlertOtpLupas(res.data.Pesan);
            setAlertOtpLupas(true);
          } else {
            handleCloseOtpLupas();
            handleShowPwLupas();
          }
        });
      }
    } catch (error) {
      if (!error.responses) {
        setPesanAlertOtpLupas("Server not found");
      }
    }
  };
  let dataLupasPassword = JSON.stringify({
    Password: passwordLupas1,
    Email: cekMailLupas
  })
  const handleSumbitLupasPW = (e) => {
    e.preventDefault();
    try {
      if (passwordLupas1 == passwordLupas2) {
        if (
          validator.isStrongPassword(passwordLupas1, {
            minLength: 8,
            minLowercase: 1,
            minUppercase: 1,
            minNumbers: 1,
            minSymbols: 1,
          })
        ) {
          axios.post("/updatepassword", dataLupasPassword);
          handleClosePwLupas();
        } else {
          setAlertPassLupas(true);
          setPesanPassLupas("Password Kurang Kuat");
        }
      } else {
        setAlertPassLupas(true);
        setPesanPassLupas("Password tidak sama");
      }
    } catch (error) {
      if (!error.responses) {
        setPesanPassLupas("Server not found");
      }
    }
  };

  return (
    <>
      <Button variant="primary" onClick={handleShowReg} className="me-2">
        Daftar
      </Button>

      <Button variant="outline-secondary" onClick={handleShow}>
        Masuk
      </Button>

      {/* Login */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Masuk</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Alert
            onClose={() => setAlert3(false)}
            show={alert3}
            variant="danger"
          >
            {pesan}
          </Alert>
          <Form size="sm" onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                size="md"
                placeholder="abcd@mail.com"
                onChange={(e) => setEmail(e.target.value)}
              />
              <Alert
                onClose={() => setAlert1(false)}
                show={alert1}
                variant="danger"
              >
                {pesanEmail}
              </Alert>
            </Form.Group>

            <Form.Group className="mb-4" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <div className="row">
                <div className="d-flex col-6-md">
                  <Form.Control
                    type={see}
                    size="md"
                    value={password}
                    placeholder="*******"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <a
                    className="btn btn-outline-secondary btn-md"
                    onClick={TogglePass}
                  >
                    {see === "password" ? <FaEye /> : <FaEyeSlash />}
                  </a>
                </div>

                <div className="col-2 ms-0"></div>
              </div>
              <Alert
                onClose={() => setAlert2(false)}
                show={alert2}
                variant="danger"
              >
                {pesanPass}
              </Alert>
            </Form.Group>
            <div className="text-center">
              <Button
                variant="primary"
                size="lg"
                type="submit"
                className="mb-4"
              >
                Masuk
              </Button>
              <br />
              <p className="mb-1">
                <Link
                  className="text-decoration-none"
                  onClick={handleLinkShowLupas}
                >
                  Lupa Password ?
                </Link>
              </p>
              <p className="mb-1">atau</p>
              <p className="mb-1">
                Belum Memiliki Akun?
                <Link className="text-decoration-none" onClick={handleLinkShow}>
                  Daftar
                </Link>
              </p>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      {/* CekMail */}
      <Modal show={showReg} onHide={handleCloseReg}>
        <Modal.Header closeButton>
          <Modal.Title>Daftar</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 className="mb-4">
            Masukkan email anda. Kami akan melakukan pengecekan
          </h5>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                onChange={(e) => setCekMail(e.target.value)}
              />
              <Alert
                onClose={() => setAlertReg(false)}
                show={alertReg}
                variant="danger"
              >
                {pesanCekMail}
              </Alert>
            </Form.Group>
            <div className="text-center">
              <Button
                variant="primary"
                size="lg"
                type="submit"
                onClick={handleCekEmail}
              >
                Kirim OTP
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      {/* OTP */}
      <Modal show={showOtp} onHide={handleCloseOtp}>
        <Modal.Header closeButton>
          <Modal.Title>Vertifikasi Email</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 className="mb-4">
            Masukkan kode OTP yang telah dikirimkan ke email anda
          </h5>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="text"
                className="text-center"
                onChange={(e) => setDataOtp(e.target.value)}
                placeholder="12345123"
              />
              <Alert
                onClose={() => setAlertOtp(false)}
                show={alertOtp}
                variant="danger"
              >
                {pesanAlertOtp}
              </Alert>
            </Form.Group>
            <div className="text-center">
              
              <Button
                className="mt-2"
                variant="primary"
                type="submit"
                onClick={handleOtp}
              >
                Konfirmasi OTP
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      {/* Password */}
      <Modal show={showPw} onHide={handleClosePw}>
        <Modal.Header closeButton>
          <Modal.Title>Daftar</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 className="mb-4">Masukkan password baru untuk akun anda</h5>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Password</Form.Label>
              <div className="row">
                <div className="d-flex col-6-md">
                  <Form.Control
                    type={see}
                    size="md"
                    value={passwordReg1}
                    placeholder="*******"
                    onChange={(e) => setPasswordReg1(e.target.value)}
                  />
                  <a
                    className="btn btn-outline-secondary btn-md"
                    onClick={TogglePass}
                  >
                    {see === "password" ? <FaEye /> : <FaEyeSlash />}
                  </a>
                </div>
                <div className="col-2 ms-0"></div>
              </div>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Konfirmasi Password</Form.Label>
              <div className="row">
                <div className="d-flex col-6-md">
                  <Form.Control
                    type={see}
                    size="md"
                    value={passwordReg2}
                    placeholder="*******"
                    onChange={(e) => setPasswordReg2(e.target.value)}
                  />
                  <a
                    className="btn btn-outline-secondary btn-md"
                    onClick={TogglePass}
                  >
                    {see === "password" ? <FaEye /> : <FaEyeSlash />}
                  </a>
                </div>

                <div className="col-2 ms-0"></div>
              </div>
              <Alert
                onClose={() => setAlertPass(false)}
                show={alertPass}
                variant="danger"
              >
                {pesanPassReg}
              </Alert>
            </Form.Group>
            <div className="text-center">
              <Button variant="primary" type="submit" onClick={handleBio}>
                Set Password
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      {/* Biodata */}
      <Modal show={showBio} onHide={handleCloseBio}>
        <Modal.Header closeButton>
          <Modal.Title>Masuk</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Alert
            onClose={() => setAlert3(false)}
            show={alert3}
            variant="danger"
          >
            {pesan}
          </Alert>
          <Form size="sm" onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Nama Lengkap</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nama Lengkap"
                onChange={(e) => setFullname(e.target.value)}
              />
              <Alert
                onClose={() => setAlertReg(false)}
                show={alertReg}
                variant="danger"
              >
                {pesan}
              </Alert>
            </Form.Group>
            <Form.Group className="mb-3" controlId="phone">
              <Form.Label>Nomor Handphone</Form.Label>
              <InputGroup className="mb-3">
                <InputGroup.Text id="basic">+62</InputGroup.Text>
                <Form.Control
                  placeholder="8371201203"
                  onChange={(e) => setNope(e.target.value)}
                />
              </InputGroup>
            </Form.Group>

            <Form.Group className="mb-3" controlId="dob">
              <Form.Label>Daftar Sebagai</Form.Label>
              <Form.Select
                defaultValue="default"
                onChange={(e) => setIdRole(e.target.value)}
              >
                <option value="default" disabled>
                  --Pilih--
                </option>
                {post.map((post) => (
                  <option key={post.Id} value={post.Id}>
                    {post.Name}
                  </option>
                ))}
              </Form.Select>
            </Form.Group>

            <div className="text-center">
              <Button
                variant="primary"
                size="lg"
                type="submit"
                className="mb-4"
                onClick={handleSubmitBio}
              >
                Daftar
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      {/* cekemailpassword */}
      <Modal show={showLupas} onHide={handleCloseLupas}>
        <Modal.Header closeButton>
          <Modal.Title>Lupas Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 className="mb-4">
            Masukkan email anda. Kami akan melakukan pengecekan
          </h5>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                onChange={(e) => setCekMailLupas(e.target.value)}
              />
              <Alert
                onClose={() => setAlertLupas(false)}
                show={alertLupas}
                variant="danger"
              >
                {pesanLupas}
              </Alert>
            </Form.Group>
            <div className="text-center">
              <Button
                variant="primary"
                size="lg"
                type="submit"
                onClick={handleCekMailLupas}
              >
                Kirim OTP
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      {/* cek otp */}
      <Modal show={showOtpLupas} onHide={handleCloseOtpLupas}>
        <Modal.Header closeButton>
          <Modal.Title>Vertifikasi Email</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 className="mb-4">
            Masukkan kode OTP yang telah dikirimkan ke email anda
          </h5>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="text"
                className="text-center"
                onChange={(e) => setDataOtpLupas(e.target.value)}
                placeholder="12345123"
              />
              <Alert
                onClose={() => setAlertOtpLupas(false)}
                show={alertOtpLupas}
                variant="danger"
              >
                {pesanAlertOtpLupas}
              </Alert>
            </Form.Group>
            <div className="text-center">
              {/* {status === STATUS.STARTED ? (
                <p>
                  Kirim Ulang dalam :{twoDigits(minutesToDisplay)} :{" "}
                  {twoDigits(secondsToDisplay)}
                </p>
              ) : (
                status
              )} */}
              <Button
                className="mt-2"
                variant="primary"
                type="submit"
                onClick={handleCekOtpLupas}
              >
                Konfirmasi OTP
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      {/* ganti password */}
      <Modal show={showPwLupas} onHide={handleClosePwLupas}>
        <Modal.Header closeButton>
          <Modal.Title>Daftar</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 className="mb-4">Masukkan password baru untuk akun anda</h5>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Password</Form.Label>
              <div className="row">
                <div className="d-flex col-6-md">
                  <Form.Control
                    type={see}
                    size="md"
                    value={passwordLupas1}
                    placeholder="*******"
                    onChange={(e) => setPasswordLupas1(e.target.value)}
                  />
                  <a
                    className="btn btn-outline-secondary btn-md"
                    onClick={TogglePass}
                  >
                    {see === "password" ? <FaEye /> : <FaEyeSlash />}
                  </a>
                </div>
                <div className="col-2 ms-0"></div>
              </div>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Konfirmasi Password</Form.Label>
              <div className="row">
                <div className="d-flex col-6-md">
                  <Form.Control
                    type={see}
                    size="md"
                    value={passwordLupas2}
                    placeholder="*******"
                    onChange={(e) => setPasswordLupas2(e.target.value)}
                  />
                  <a
                    className="btn btn-outline-secondary btn-md"
                    onClick={TogglePass}
                  >
                    {see === "password" ? <FaEye /> : <FaEyeSlash />}
                  </a>
                </div>

                <div className="col-2 ms-0"></div>
              </div>
              <Alert
                onClose={() => setAlertPassLupas(false)}
                show={alertPassLupas}
                variant="danger"
              >
                {pesanPassLupas}
              </Alert>
            </Form.Group>
            <div className="text-center">
              <Button
                variant="primary"
                type="submit"
                onClick={handleSumbitLupasPW}
              >
                Set Password
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Login;
