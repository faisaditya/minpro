import React, {useState,useEffect} from 'react'

function CountDown() {
  const [timeLeft,setTimeLeft] = useState(3 * 60 * 1000);
  useEffect(()=>{
    setTimeout(()=>{
      setTimeLeft(timeLeft - 1000)
    }, 1000)
    if (timeLeft >= 0) {
      setTimeLeft(0)
    }
  },[timeLeft])
  const getFormattedTime = (milliseconds) =>{
    let total_second = parseInt(Math.floor(milliseconds/1000))
    let total_menutes = parseInt(Math.floor(total_second/60))
    let seconds = parseInt(total_second % 60)
    let menutes = parseInt(total_menutes % 60)
  
    return `${menutes}:${seconds}`
  }
  return (
    <div>{getFormattedTime(timeLeft)}</div>
  )
}

export default CountDown




