import React, { Component } from "react";
import { FaBriefcaseMedical, FaShoppingCart } from "react-icons/fa";
import { Link } from "react-router-dom";

const SideBar = () => {
  let data = sessionStorage.getItem('bio')
  
    return (
      <>
        <div
          className="sidebar elevation-1 h-100"
          style={{ backgroundColor: "#C7EEFB" }}
        >
          {/* Sidebar */}
          <div className="sidebar">
            {/* Sidebar Menu */}
            <nav className="mt-2">
              <ul
                className="nav nav-pills nav-sidebar flex-column"
                data-widget="treeview"
                role="menu"
                data-accordion="false"
              >
                <li className="nav-item">
                  <Link to="" className="nav-link active">
                    <span
                      className="items-center"
                      style={{ display: "flex", alignItems: "center" }}
                    >
                      <FaBriefcaseMedical className="mr-2" size={25} />
                      <span>Menu 1</span>
                    </span>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="" className="nav-link ">
                    <span style={{ display: "flex", alignItems: "center" }}>
                      <FaBriefcaseMedical className="mr-2" size={25} />
                      <span>Menu 2</span>
                    </span>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="" className="nav-link ">
                    <span
                      className=""
                      style={{ display: "flex", alignItems: "center" }}
                    >
                      <FaShoppingCart className="mr-2" size={25} />
                      <span>Menu 3</span>
                    </span>
                  </Link>
                </li>
              </ul>
            </nav>
            {/* /.sidebar-menu */}
          </div>

          {/* /.sidebar */}
        </div>
      </>
    );
  }

export default SideBar;
