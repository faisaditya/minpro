import React from "react";
import { FaClipboardCheck, FaPen } from "react-icons/fa";
import { Link } from "react-router-dom";
import Layout from './Layout'

const Menu = ()=> {
    return (
      <Layout>
        <div className="container my-5">
          <div className="row">
            {/* card 1  */}
            <div className="col-3 mb-5">
              <Link to="">
                <div
                  class="card shadow p-5 mb-3 bg-body rounded"
                  style={{ display: "flex", alignItems: "center" }}
                >
                  <FaPen size={"50px"} />
                </div>
              </Link>
              <div className="card-body mb-3">
                <h5 className="text-center">Nama Menu</h5>
              </div>
            </div>

            {/* card 2  */}
            <div className="col-3">
              <Link to="" onClick="">
                <div
                  className="card kartu shadow p-5 mb-3 bg-body rounded"
                  style={{ display: "flex", alignItems: "center" }}
                >
                  {/* <img
                    className="rounded-circle mx-auto d-block mt-4"
                    src="assets/img/icon/search_black_48dp.svg"
                    width="30%"
                    alt="menu"
                  /> */}
                  <FaClipboardCheck size={"50px"} />
                </div>
              </Link>
              <div className="card-body mb-3">
                <h5 className="text-center">Nama Menu</h5>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
    
}

export default Menu