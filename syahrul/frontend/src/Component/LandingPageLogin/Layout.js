import React from "react";
import Header from "../LandingPageLogin/Header";
import SideBar from "../LandingPageLogin/SideBar";

function Main(props) {
  const { children } = props;

  return (
    <div className="wrapper">
      <div id="content-wrapper" className=" d-flex flex-column">
        <div className="content">
          <Header />
          <div className="d-flex flex-row">
            <div className="flex">
              <SideBar />
            </div>
            <div className="col align-self-center">{children}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Main;
