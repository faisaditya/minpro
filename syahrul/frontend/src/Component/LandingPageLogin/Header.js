import React, { useState,useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "../Api/Api";

const Header = () => {
  let userData = localStorage.getItem('bio')

  const [post, setPost] = useState("")
  const [image,setImage] = useState("")

  useEffect(() => {
    axios
      .get("http://localhost/api/profile/"+userData)
      .then((res) => {
        setPost(res.data.fullname);
        setImage(res.data.image_path)
      })
      .catch((err) => {
        console.log(err);
      });
  },);

  let onLogout = () => {
    localStorage.removeItem('role')
    localStorage.removeItem('bio')
    localStorage.removeItem('login')  
}
  
    return (
      <>
        <nav className="navbar p-2" style={{ backgroundColor: "#C7EEFB" }}>
          <div className="container ">
            <Link to="/" className="navbar-brand text-decoration-none">
              <div className="fs-2 fw-bold text-primary">
                <img
                  src="assets/img/logo.png"
                  width="36"
                  height="36"
                  alt="Logo"
                  className="brand-image img-circle elevation-2 bg-primary"
                  style={{ opacity: ".8" }}
                />
                <span className="ml-2">Med.id</span>
              </div>
            </Link>
            <div class="header-middle col-sm-6">
              <div className="input-group input-group-sm">
                <input
                  className="form-control form-control-navbar"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                />
                <div className="input-group-append">
                  <button className="btn btn-navbar bg-primary" type="submit">
                    <i className="fas fa-search" />
                  </button>
                </div>
              </div>
            </div>
            <div className="header-right col-sm-2">
              <ul class="nav user-menu header-right">
                <li class="nav-item dropdown has-arrow">
                  <Link
                    to="#"
                    class="dropdown-toggle nav-link user-link"
                    data-toggle="dropdown"
                  >
                    <span class="user-img">
                      <img
                        class="rounded-circle mr-2"
                        src={image ? image : "default.png"}
                        width="40"
                        alt="img profile"
                      />
                    </span>
                    <span>Halo!, {post}</span>
                  </Link>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="/profile">
                      My Profile
                    </a>
                    <a class="dropdown-item" href="/" onClick={onLogout}>
                      Logout
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </>
    );
}

export default Header;
