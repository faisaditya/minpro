import React from "react";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card from "./CardProfile";
import TabProfile from "./TabProfile";


function Profile() {
  return (
    <>
      <Breadcrumb
        style={{ backgroundColor: "#C7EEFB" }}
        className="pt-3 pb-2 ps-5"
      >
        <Breadcrumb.Item href="/dashboard">Home</Breadcrumb.Item>
        <Breadcrumb.Item active>Profile</Breadcrumb.Item>
      </Breadcrumb>
      <div className="container-fluid col-12 bg-light">
        <div className="row">
          <div className="col-lg-3 pt-5 ms-4 me-4 bg-light">
            <Card />
          </div>
          <div className="col-lg-8 bg-light pt-5 ms-4 ">
            <TabProfile />
          </div>
        </div>
      </div>
    </>
  );
}

export default Profile;
