import React, { useEffect, useState } from "react";
import Card from "react-bootstrap/Card";
import { Image } from "react-bootstrap";
import { FaStar } from "react-icons/fa";
import MenuProfile from "./MenuProfile";
import axios from "../Api/Api";
import ModalUpload from "./ModalUpload";

function CardProfile() {
  let data = localStorage.getItem("bio");
  const [imagePath, setImagePath] = useState("");
  const [since, setSince] = useState("");
  const [name, setName] = useState("");

  useEffect(() => {
    axios
      .get("/profile/" + data)
      .then((res) => {
        setImagePath(res.data.image_path);
        setSince(res.data.since);
        setName(res.data.fullname);
      })
      .catch((err) => {
        console.log(err);
      });
  });

  return (
    <>
      <Card>
        <div className="d-flex justify-content-end mt-2 me-2">
          <ModalUpload />
        </div>
        <Image variant="top" className="d-flex flex-column align-self-center" roundedCircle src={imagePath ? imagePath : "default.png"} width="170px" height={160} />
        <Card.Body>
          <FaStar />
          <h5>{name}</h5>
          <p className="h6">Bronze Member</p>
          <p className="h6">Since {since}</p>

          <MenuProfile />
        </Card.Body>
      </Card>
    </>
  );
}

export default CardProfile;
