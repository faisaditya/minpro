import React, { useState } from "react";
import { FaPencilAlt } from "react-icons/fa";
import { Form, Alert } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import axios from "../Api/Api";

function ModalUpload() {
  const [show, setShow] = useState(false);
  let id = localStorage.getItem("bio");
  
  const handleShow = () => setShow(true);
  const [file, setFile] = useState();
  const [pesan, setPesan] = useState("");
  const [alert, setAlert] = useState(false);

  const handleClose = () => {
    setShow(false);
    setFile();
    setAlert(false);
    setPesan("");
  };
  
  const handleChange = (e) => {
    setFile(e.target.files[0]);
  };
  const isValidFileUploaded = (file) => {
    const validExtensions = ["png", "jpeg", "jpg"];
    const fileExtension = file.type.split("/")[1];
    return validExtensions.includes(fileExtension);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (!file) {
      setPesan("Masukkan File Terlebih dahulu")
      setAlert(true)
    } else if (!isValidFileUploaded(file)) {
      setPesan("File Bukan Images");
      setAlert(true);
    } else {
      const formData = new FormData();
      formData.append("file", file);
      formData.append("user", id);
      axios.post("/upload", formData, id).then((res) => {
        console.log(res.data);
      });
      handleClose();
    }
  };

  return (
    <div>
      <Button
        size="sm"
        className="d-flex justify-content-between"
        onClick={handleShow}
      >
        <FaPencilAlt />
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Upload Photo</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId="formFileMultiple" className="mb-3">
              <Form.Label>Pilih Berkas Photo</Form.Label>
              <Form.Control type="file" onChange={handleChange} multiple />
              <Alert
                onClose={() => setAlert(false)}
                show={alert}
                variant="danger"
              >
                {pesan}
              </Alert>
            </Form.Group>
            <Button size="md" type="submit">
              Simpan
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalUpload;
