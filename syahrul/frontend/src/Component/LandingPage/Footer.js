import React from "react";
import { Link } from "react-router-dom";

function Footer() {
  return (
    <>
      <footer className="bg-light text-center text-lg-start">
        <div class="text-center p-3" style={{ backgroundColor: "#C7EEFB" }}>
          © 2022 Copyright
          <Link className="text-decoration-none text-bold ml-1" to="/">
            Team A
          </Link>
        </div>
      </footer>
    </>
  );
}

export default Footer;
