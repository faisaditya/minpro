import React from "react";
import HeaderNotLogin from "./HeaderNotLogin";
import Footer from "./Footer";

function LandingPage(props) {

  const { children } = props;
  return (
    <>
      <div className="wrapper">
        <div id="content-wrapper" className=" d-flex flex-column">
          <div className="content">
            <HeaderNotLogin />
            <div className="d-flex flex-row">
              <div className="col align-self-center">{children}</div>
            </div>
            <Footer />
          </div>
        </div>
      </div>
    </>
  );
}

export default LandingPage;
