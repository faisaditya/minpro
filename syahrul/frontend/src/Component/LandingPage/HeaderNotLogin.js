import React from "react";
import { Link } from "react-router-dom";
import Login from "../Login/Login";


const HeaderNotLogin = (props) => {
  return (
    <>
      <nav className="navbar p-4 " style={{ backgroundColor: "#C7EEFB" }}>
        <div className="container ">
          <Link to="/" className="navbar-brand text-decoration-none">
            <div className="fs-2 fw-bold text-primary">
              <img
                src="assets/img/logo.png"
                width="36"
                height="36"
                alt="Logo"
                className="brand-image img-circle elevation-2 bg-primary"
                style={{ opacity: ".8" }}
              />
              <span className="ml-2">Med.id</span>
            </div>
          </Link>
          <div className="header-middle col-sm-6">
            <div className="input-group input-group-sm">
              <input
                className="form-control form-control-navbar"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <div className="input-group-append">
                <button className="btn btn-navbar bg-primary" type="submit">
                  <i className="fas fa-search" />
                </button>
              </div>
            </div>
          </div>
          <div className="header-right col-sm-2">
            <Login />
          </div>
        </div>
      </nav>
    </>
  );
};

export default HeaderNotLogin;
