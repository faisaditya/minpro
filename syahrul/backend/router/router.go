package router

import (
	"backend/controllers"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {
	router := mux.NewRouter()
	//membuat subrouter
	pre := router.PathPrefix("/api").Subrouter()

	//ambil data login
	pre.HandleFunc("/user", controllers.GetDataLogin).Methods("POST")

	pre.HandleFunc("/profile/{id}", controllers.GetBiodata).Methods("GET")

	pre.HandleFunc("/upload", controllers.UploadFile).Methods("POST")

	pre.HandleFunc("/cekemail", controllers.CariData).Methods("POST")

	pre.HandleFunc("/cekotp", controllers.CekOtp).Methods("POST")

	pre.HandleFunc("/resend", controllers.ResendOtp).Methods("POST")

	pre.HandleFunc("/setpass", controllers.UpdatePass).Methods("POST")

	pre.HandleFunc("/getrole", controllers.GetRole).Methods("GET")

	pre.HandleFunc("/setbio", controllers.SetBio).Methods("POST")
	
	pre.HandleFunc("/ceklupas", controllers.CekMailLupas).Methods("POST")

	pre.HandleFunc("/cekotplupas",controllers.CekOtpPw).Methods("POST")

	pre.HandleFunc("/updatepassword",controllers.UpdatePassPw).Methods("POST")

	return router
}
