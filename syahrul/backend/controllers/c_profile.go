package controllers

import (
	"backend/models"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/teris-io/shortid"
)

func GetBiodata(w http.ResponseWriter, r *http.Request) {
	Data := mux.Vars(r) //mengambil data id dari link
	var id string = Data["id"]
	convert, _ := strconv.Atoi(id)
	marshal, _ := json.MarshalIndent(models.GetBiodata(convert), "", "\t")
	w.Write(marshal)
}


func UploadFile(w http.ResponseWriter, r *http.Request) {
	refid, _ := shortid.Generate()
	id := r.FormValue("user")
	uploadedFile, handler, err := r.FormFile("file")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	
	defer uploadedFile.Close()
	
	// dir, err := os.Getwd()
	// if err != nil {
	// 	http.Error(w, err.Error(), http.StatusInternalServerError)
	// 	return
	// }
	filename := handler.Filename
	if refid != "" {
		filename = fmt.Sprintf("%s%s", refid, filepath.Ext(handler.Filename))
	}
	fileLocation := filepath.Join( "C:/batch300Golang/teama_batch300/syahrul/frontend/public", filename)
	targetFile, err := os.OpenFile(fileLocation, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer targetFile.Close()

	if _, err := io.Copy(targetFile, uploadedFile); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	models.UploadFile(filename, id)
}