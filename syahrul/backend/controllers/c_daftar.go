package controllers

import (
	"backend/models"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"

	"gopkg.in/gomail.v2"
)

const CONFIG_SMTP_HOST = "smtp.gmail.com"
const CONFIG_SMTP_PORT = 587
const CONFIG_SENDER_NAME = "syahrulr980104@gmail.com"
const CONFIG_AUTH_EMAIL = "syahrulr980104@gmail.com"
const CONFIG_AUTH_PASSWORD = "xswkmkphdqzqmxmi"

var OTP int

func CariData(w http.ResponseWriter, r *http.Request) {
	var email models.Email
	var pesan models.Msg
	OTP = rand.Intn(99999)
	err := json.NewDecoder(r.Body).Decode(&email)
	if err != nil {
		fmt.Println("error Decoding ", err)
	}
	data := models.CariData(email)

	if data == "" {
		otp := strconv.Itoa(OTP)
		sendEmail := gomail.NewMessage()
		sendEmail.SetHeader("From", CONFIG_SENDER_NAME)
		sendEmail.SetHeader("To", email.Email)
		sendEmail.SetHeader("Subject", "Vertifikasi Akun")
		sendEmail.SetBody("text/html", "Kode OTP anda adalah "+otp+" Kode OTP akan hangus jika sudah lebih dari 10 menit")

		setMail := gomail.NewDialer(
			CONFIG_SMTP_HOST,
			CONFIG_SMTP_PORT,
			CONFIG_AUTH_EMAIL,
			CONFIG_AUTH_PASSWORD,
		)
		err := setMail.DialAndSend(sendEmail)
		if err != nil {
			fmt.Println("gagal mengirim", err)
		} else {
			pesan = models.Msg{
				Status: false,
				Pesan:  "OTP Telah Terkirim",
			}
		}
	} else {
		pesan = models.Msg{
			Status: true,
			Pesan:  "Email Sudah Terdaftar",
		}
	}
	json.NewEncoder(w).Encode(pesan)
}

func ResendOtp(w http.ResponseWriter, r *http.Request) {
	var email models.Email
	var pesan models.Msg
	err := json.NewDecoder(r.Body).Decode(&email)
	if err != nil {
		fmt.Println("error Decode Resend :", err)
	}
	OTP = rand.Intn(999999)
	otp := strconv.Itoa(OTP)
	sendEmail := gomail.NewMessage()
	sendEmail.SetHeader("From", CONFIG_SENDER_NAME)
	sendEmail.SetHeader("To", email.Email)
	sendEmail.SetHeader("Subject", "Vertifikasi Akun")
	sendEmail.SetBody("text/html", "Kode OTP anda adalah "+otp+" Kode OTP akan hangus jika sudah lebih dari 10 menit")

	setMail := gomail.NewDialer(
		CONFIG_SMTP_HOST,
		CONFIG_SMTP_PORT,
		CONFIG_AUTH_EMAIL,
		CONFIG_AUTH_PASSWORD,
	)
	err1 := setMail.DialAndSend(sendEmail)
	if err1 != nil {
		fmt.Println("gagal mengirim", err)
	}
	pesan = models.Msg{
		Pesan: "OTP Telah Terkirim",
	}
	json.NewEncoder(w).Encode(pesan)
}

func CekOtp(w http.ResponseWriter, r *http.Request) {
	var dataOtp models.Otp
	var pesan models.Msg
	err := json.NewDecoder(r.Body).Decode(&dataOtp)
	if err != nil {
		fmt.Println("error json decode otp :", err)
	}
	if dataOtp.Otp == OTP {
		models.Daftar(dataOtp.Email)
		pesan = models.Msg{
			Status: false,
			Pesan:  "Otp Sukses",
		}
	} else {
		pesan = models.Msg{
			Status: true,
			Pesan:  "Kode Otp salah",
		}
	}
	json.NewEncoder(w).Encode(pesan)
}

func UpdatePass(w http.ResponseWriter, r *http.Request) {
	var dataPass models.SetPass
	err := json.NewDecoder(r.Body).Decode(&dataPass)
	if err != nil {
		fmt.Println("error json decode update pass", err)
	}
	models.SetPassword(dataPass)
}

func GetRole(w http.ResponseWriter, r *http.Request) {
	marshal, _ := json.MarshalIndent(models.GetRole(), "", "\t")
	w.Write(marshal)
}

func SetBio(w http.ResponseWriter, r *http.Request) {
	var dataBio models.Bio
	err := json.NewDecoder(r.Body).Decode(&dataBio)
	if err != nil {
		fmt.Println("error decode :", err)
	}
	models.SetBio(dataBio)
}
