package controllers

import (
	"backend/models"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"

	"gopkg.in/gomail.v2"
)

var OTPLupas int

func CekMailLupas(w http.ResponseWriter, r *http.Request){
	var data models.CekMailLupas
	var pesan models.Pesan
	err := json.NewDecoder(r.Body).Decode(&data)
	OTPLupas = rand.Intn(99999)
	if err != nil {
		fmt.Println("error Decoding ", err)
	}
	hasil := models.CekLupas(data)
	if hasil != ""{
		otp := strconv.Itoa(OTPLupas)
		sendEmail := gomail.NewMessage()
		sendEmail.SetHeader("From", CONFIG_SENDER_NAME)
		sendEmail.SetHeader("To", data.Email)
		sendEmail.SetHeader("Subject", "Vertifikasi Akun")
		sendEmail.SetBody("text/html", "Kode OTP anda adalah "+otp+" Kode OTP akan hangus jika sudah lebih dari 10 menit")

		setMail := gomail.NewDialer(
			CONFIG_SMTP_HOST,
			CONFIG_SMTP_PORT,
			CONFIG_AUTH_EMAIL,
			CONFIG_AUTH_PASSWORD,
		)
		err := setMail.DialAndSend(sendEmail)
		if err != nil {
			fmt.Println("gagal mengirim", err)
		}
		pesan = models.Pesan{
			Status: false,
			Pesan:  "OTP Telah Terkirim",
		}
	} else {
		pesan = models.Pesan{
			Status: true,
			Pesan:  "Email Tidak Terdaftar",
		}
	}
	json.NewEncoder(w).Encode(pesan)
}

func ResendOtpPw(w http.ResponseWriter, r *http.Request) {
	var email models.Email
	var pesan models.Msg
	err := json.NewDecoder(r.Body).Decode(&email)
	if err != nil {
		fmt.Println("error Decode Resend :", err)
	}
	OTPLupas = rand.Intn(999999)
	otp := strconv.Itoa(OTPLupas)
	sendEmail := gomail.NewMessage()
	sendEmail.SetHeader("From", CONFIG_SENDER_NAME)
	sendEmail.SetHeader("To", email.Email)
	sendEmail.SetHeader("Subject", "Vertifikasi Akun")
	sendEmail.SetBody("text/html", "Kode OTP anda adalah "+otp+" Kode OTP akan hangus jika sudah lebih dari 10 menit")

	setMail := gomail.NewDialer(
		CONFIG_SMTP_HOST,
		CONFIG_SMTP_PORT,
		CONFIG_AUTH_EMAIL,
		CONFIG_AUTH_PASSWORD,
	)
	err1 := setMail.DialAndSend(sendEmail)
	if err1 != nil {
		fmt.Println("gagal mengirim", err)
	}
	pesan = models.Msg{
		Pesan: "OTP Telah Terkirim",
	}
	json.NewEncoder(w).Encode(pesan)
}

func CekOtpPw(w http.ResponseWriter, r *http.Request) {
	var dataOtp models.Otp
	var pesan models.Pesan
	err := json.NewDecoder(r.Body).Decode(&dataOtp)
	if err != nil {
		fmt.Println("error json decode otp :", err)
	}
	if dataOtp.Otp == OTPLupas {
		data :=models.GetIdUser(dataOtp.Email)
		pesan = models.Pesan{
			Status: false,
			Pesan:  "Otp Sukses",
			Id: data,
		}
	} else {
		pesan = models.Pesan{
			Status: true,
			Pesan:  "Kode Otp salah",
		}
	}
	json.NewEncoder(w).Encode(pesan)
}

func UpdatePassPw(w http.ResponseWriter, r *http.Request) {
	var dataPass models.SetData
	err := json.NewDecoder(r.Body).Decode(&dataPass)
	if err != nil {
		fmt.Println("error json decode update pass", err)
	}
	models.SetPassLupas(dataPass.Email,dataPass.Password)
}