module backend

go 1.19

require github.com/gorilla/handlers v1.5.1

require gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect

require (
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.7
	github.com/teris-io/shortid v0.0.0-20220617161101-71ec9f2aa569
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
