package models

import (
	"backend/config"
	"fmt"
)

type DataLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Login struct {
	Role_id    int `json:"role_id"`
	Biodata_id int `json:"biodata_id"`
}
type Attempt struct {
	Login_attempt int
	Id            int
	Is_locked bool
}

func GetUser(data DataLogin) Login {
	var login Login
	var dataLogin Attempt
	data1 := dataLogin.Login_attempt
	db := config.ConnectDB()
	defer db.Close()
	sql := "select login_attempt,id from m_user where lower(email) = lower($1)"
	row, err := db.Query(sql, data.Email)
	if err != nil {
		fmt.Println("error sql :", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&dataLogin.Login_attempt, &dataLogin.Id)
	}
	data2 := data1 + 1
	fmt.Println(data2, ": ", dataLogin.Id)
	if row != nil {
		sql1 := "select role_id,biodata_id from m_user where lower(email) = lower($1) and password = $2"
		row2, err := db.Query(sql1, data.Email, data.Password)
		if err != nil {
			fmt.Println("err sql 2 :", err)
		}
		defer row2.Close()
		if row2 != nil {
			for row2.Next() {
				row2.Scan(&login.Role_id, &login.Biodata_id)
			}
		} else {
			sql2 := "update m_user set login_attempt = $1 where id=$2"
			_, err := db.Exec(sql2, data2, dataLogin.Id)
			if err != nil {
				fmt.Println("err sql 3 :", err)
			}
		}
	}

	return login
}

func GetData(email string) Attempt {
	var attampt Attempt
	db := config.ConnectDB()
	defer db.Close()
	sql := "select id,login_attempt,is_locked from m_user where lower(email) = lower($1) and is_delete = false"
	row, err := db.Query(sql, email)
	if err != nil {
		fmt.Println("error Connect GetData :", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&attampt.Id, &attampt.Login_attempt, &attampt.Is_locked)
	}
	return attampt
}

func GetPassword(data DataLogin) Login {
	var result Login
	db := config.ConnectDB()
	defer db.Close()
	sql := "select role_id,biodata_id from m_user where lower(email) = lower($1) and password = $2 and is_delete = false"
	row, err := db.Query(sql, data.Email, data.Password)
	if err != nil {
		fmt.Println("error Connect GetData :", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&result.Role_id,&result.Biodata_id)
	}
	return result
}

func AddAttempt(id int, attm int) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "update m_user set login_attempt = $1, modified_by=$2, modified_on=now() where id = $3"
	_, err := db.Exec(sql, attm, id, id)
	if err != nil {
		fmt.Println("error Connect GetData :", err)
	}
}

func GetAttempt(id int)int{
	var data int
	db := config.ConnectDB()
	defer db.Close()
	sql := "select login_attempt from m_user where id = $1"
	row,err := db.Query(sql,id)
	if err != nil {
		fmt.Println("error db get attempt :", err)
	}
	for row.Next() {
		row.Scan(&data)
	}
	return data
}

func IsBlocked(id int) {
	var data = true
	db := config.ConnectDB()
	defer db.Close()
	sql := "update m_user set is_locked = $1, modified_by=$2, modified_on=now() where id = $3"
	_, err := db.Exec(sql, data, id, id)
	if err != nil {
		fmt.Println("error Connect GetData :", err)
	}
}

func GetBloked(email string)bool{
	var status bool
	db := config.ConnectDB()
	defer db.Close()
	sql := "select is_locked from m_user where email = $1"
	row,er := db.Query(sql, email)
	if er != nil {
		fmt.Println("error Query Blocked :",er)
	}
	for row.Next() {
		row.Scan(&status)
	}
	return status
}

func LastLogin(id int){
	db := config.ConnectDB()
	defer db.Close()
	sql := "update m_user set last_login = now() where id = $1"
	_,err := db.Exec(sql,id)
	if err != nil {
		fmt.Println("error Query last login :",err)
	}
}