package models

import (
	"backend/config"
	"fmt"
)

type Email struct{
	Email string
}
type Msg struct{
	Status bool
	Pesan string
}
type Otp struct{
	Otp int
	Email string
}
type SetPass struct{
	Email string
	Password string
}
type Bio struct{
	Email string
	Fullname string
	Mobile_phone string
	Role_id int
}
type Role struct{
	Id int
	Name string
}

func CariData(str Email) string{
	var email string
	db := config.ConnectDB()
	defer db.Close()

	sql := "select email from m_user where email = $1"
	row,err := db.Query(sql, str.Email)
	if err != nil {
		fmt.Println("erro Query Cari :",err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&email)
	}
	return email
}

func Daftar(str string){
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into m_user (email,is_locked,created_by,created_on) values ($1,false,2,now())"
	_,err := db.Exec(sql, str)
	if  err != nil {
		fmt.Println("error Query Daftar :",err)
	}
}

func SetPassword(str SetPass){
	db := config.ConnectDB()
	defer db.Close()
	sql := "update m_user set password = $1 where email = $2"
	_,err := db.Exec(sql,str.Password,str.Email)
	if err != nil {
		fmt.Println("error update pass :", err)
	}
}

func SetBio(data Bio){
	SetRole(data.Role_id,data.Email)
	SetBiodata(data.Fullname,data.Mobile_phone,data.Role_id)
	idBio := GetIdBiodata(data.Fullname)
	db := config.ConnectDB()
	defer db.Close()
	sql4 := "update m_user set biodata_id = $1 where email = $2"
	_,err := db.Exec(sql4,idBio,data.Email)
	if err != nil {
		fmt.Println("error update :",err)
	}
}

func GetRole()[]Role{
	var role []Role
	db := config.ConnectDB()
	defer db.Close()
	sql := "select id,name from m_role where name <> 'Role Admin'"
	row,err := db.Query(sql)
	if err != nil {
		fmt.Println("error Query :",err)
	}
	defer row.Close()
	for row.Next() {
		var data Role
		row.Scan(&data.Id,&data.Name)
		role = append(role,data)
	}
	return role
}

func SetRole(id int, email string){
	db := config.ConnectDB()
	defer db.Close()
	sql := "update m_user set role_id = $1 where email = $2"
	_,err := db.Exec(sql,id, email)
	if err != nil {
		fmt.Println("error Exec :",err)
	}
}

func SetBiodata(name string,tlp string, role int){
	db := config.ConnectDB()
	defer db.Close()
	sql2 := "insert into m_biodata (fullname,mobile_phone,created_by,created_on) values ($1,$2,$3,now())"
	_,err := db.Exec(sql2,name,tlp,role)
	if err != nil {
		fmt.Println("error :", err)
	}
}
func GetIdBiodata(name string)int{
	db := config.ConnectDB()
	var id int
	sql3 := "select id from m_biodata where fullname = $1"
	row,err3 := db.Query(sql3,name)
	if err3 != nil {
		fmt.Println("error Data :", err3)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&id)
	}
	return id
}