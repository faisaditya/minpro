package models

import (
	"backend/config"
	"fmt"
)

type CekMailLupas struct {
	Email string
}

type Pesan struct {
	Status bool
	Pesan string
	Id int
}

type SetData struct{
	Password string
	Email string
}

func CekLupas(str CekMailLupas) string {
	db := config.ConnectDB()
	var email string
	defer db.Close()
	sql := "select email from m_user where email = $1 and is_delete=false"
	row,err := db.Query(sql,str.Email)
	if err != nil{
		fmt.Println("error Query :",err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&email)
	}
	return email
}

func GetIdUser(str string)int{
	db := config.ConnectDB()
	var data int
	defer db.Close()
	sql := "select id from m_user where email = $1"
	row,err := db.Query(sql,str)
	if err != nil {
		fmt.Println("error Get Id:",err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&data)
	}
	return data
}

func SetPassLupas(mail string, pass string){
	db := config.ConnectDB()
	defer db.Close()
	sql := "update m_user set password = $1 where email = $2"
	_,err := db.Exec(sql,pass, mail)
	if err != nil {
		fmt.Println("error set Password :",err)
	}
}