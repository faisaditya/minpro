package models

import (
	"backend/config"
	"fmt"
	"strings"
)

type Biodata struct {
	Fullname     string `json:"fullname"`
	Mobile_phone string `json:"nope"`
	Image_path   string `json:"image_path"`
	Email        string `json:"email"`
	Password     string `json:"password"`
	Dob          string `json:"dob"`
	Created_on   string `json:"since"`
}


func GetBiodata(id int) Biodata {
	var bio Biodata
	db := config.ConnectDB()
	defer db.Close()
	sql := "select dob,fullname,mobile_phone, email,password,image_path, us.created_on from m_biodata bio left join m_customer cus on bio.id = cus.biodata_id join m_user us on bio.id = us.biodata_id where bio.id=$1"
	row, err := db.Query(sql, id)
	if err != nil {
		fmt.Println("Error db profile", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&bio.Dob, &bio.Fullname, &bio.Mobile_phone, &bio.Email, &bio.Password, &bio.Image_path, &bio.Created_on)
	}
	var Bulan = bio.Dob
	arr := strings.Split(Bulan, "-")
	pecahan := arr[2][:2]
	month := bulan(arr[1])
	tgl := arr[0]
	arr[0] = pecahan
	arr[1] = month
	arr[2] = tgl
	gabung := strings.Join(arr, " ")
	bio.Dob = gabung
	tahun := bio.Created_on
	arr2 := strings.Split(tahun, "-")
	pecahTahun := arr2[0]
	bio.Created_on = pecahTahun
	return bio
}

func bulan(data string) string {
	var result string
	switch data {
	case "01":
		result = "Januari"
	case "02":
		result = "Februari"
	case "03":
		result = "Maret"
	case "04":
		result = "April"
	case "05":
		result = "Mei"
	case "06":
		result = "Juni"
	case "07":
		result = "Juli"
	case "08":
		result = "Agustus"
	case "09":
		result = "September"
	case "10":
		result = "Oktober"
	case "11":
		result = "November"
	case "12":
		result = "Desember"
	}
	return result
}

func UploadFile(str string, id string) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "update m_biodata set image_path= $1, modified_by=$2, modified_on= now() where id= $3"
	_, err := db.Exec(sql, str, id, id)
	if err != nil {
		fmt.Println("error Query Upload :", err)
	}
}
